<?php

namespace Project\Tools;

if (defined('Project\Tools\IS_START')) {
    return;
}

const IS_START = true;

use Bitrix\Main\Loader;

if (!defined('Project\Debug\IS_START') and !function_exists('isDebug')) {
    include_once(__DIR__ . '/debug.php');
}

$classPath = '/' . implode('/', array_slice(explode('/', __DIR__), -4)) . '/';
Loader::registerAutoLoadClasses(null, [
    /* Config */
    'Project\Tools\Config' => $classPath . 'config.php',

    /* Iblock */
    'Project\Tools\Iblock\V2\Update\Add\Highload' => $classPath . 'iblock/v2/update/add/highload.php',
    'Project\Tools\Iblock\V2\Update\Add\UserField' => $classPath . 'iblock/v2/update/add/userfield.php',
    'Project\Tools\Iblock\V2\Update\Highload' => $classPath . 'iblock/v2/update/highload.php',
    'Project\Tools\Iblock\V2\Highload' => $classPath . 'iblock/v2/highload.php',

    /* Iblock v2 */
    'Project\Tools\Iblock\Highload' => $classPath . 'iblock/highload.php',
    'Project\Tools\Iblock\Iblock' => $classPath . 'iblock/iblock.php',
    'Project\Tools\Iblock\Update\Highload' => $classPath . 'iblock/update/highload.php',
    'Project\Tools\Iblock\Element' => $classPath . 'iblock/element.php',
    'Project\Tools\Iblock\Property' => $classPath . 'iblock/property.php',
    'Project\Tools\Iblock\Section' => $classPath . 'iblock/section.php',
    'Project\Tools\Iblock\Tag' => $classPath . 'iblock/tag.php',
    'Project\Tools\Iblock\Type' => $classPath . 'iblock/type.php',
    'Project\Tools\Iblock\Update\Add' => $classPath . 'iblock/update/add.php',
    'Project\Tools\Iblock\Update\Catalog' => $classPath . 'iblock/update/catalog.php',
    'Project\Tools\Iblock\Update\Iblock' => $classPath . 'iblock/update/iblock.php',
    'Project\Tools\Iblock\Update\Property' => $classPath . 'iblock/update/property.php',
    'Project\Tools\Iblock\Update\Section' => $classPath . 'iblock/update/section.php',

    /* Image */
    'Project\Tools\Image\File' => $classPath . 'image/file.php',
    'Project\Tools\Image\Image' => $classPath . 'image/image.php',
    'Project\Tools\Image\Imagemagick' => $classPath . 'image/imagemagick.php',
    'Project\Tools\Image\Optimize' => $classPath . 'image/optimize.php',
    'Project\Tools\Image\Svg' => $classPath . 'image/svg.php',
    'Project\Tools\Image\Watermark' => $classPath . 'image/watermark.php',
    'Project\Tools\Image\Webp' => $classPath . 'image/webp.php',

    /* Log */
    'Project\Tools\Log\Report' => $classPath . 'log/report.php',

    /* Model */
    'Project\Tools\Model\Data' => $classPath . 'model/data.php',

    /* Modules */
    'Project\Tools\Modules\Install' => $classPath . 'modules/install.php',
    'Project\Tools\Modules\Utility' => $classPath . 'modules/utility.php',

    /* Orm */
    'Project\Tools\Orm\Database\Section' => $classPath . 'orm/database/section.php',
    'Project\Tools\Orm\Entity\Query' => $classPath . 'orm/entity/query.php',
    'Project\Tools\Orm\TraitList\Bigdata' => $classPath . 'orm/traitlist/bigdata.php',
    'Project\Tools\Orm\TraitList\Query' => $classPath . 'orm/traitlist/query.php',

    /* Parse */
    'Project\Tools\Parse\Content' => $classPath . 'parse/content.php',
    'Project\Tools\Parse\Image' => $classPath . 'parse/image.php',

    /* Sale */
    'Project\Tools\Sale\Discount' => $classPath . 'sale/discount.php',

    /* Seo */
    'Project\Tools\Seo\Typograf' => $classPath . 'seo/typograf.php',

    /* Trains */
    'Project\Tools\Trains\Event' => $classPath . 'trains/event.php',
    'Project\Tools\Trains\Agent' => $classPath . 'trains/agent.php',

    /* Utility */
    'Project\Tools\Utility\Agent' => $classPath . 'utility/agent.php',
    'Project\Tools\Utility\Cache' => $classPath . 'utility/cache.php',
    'Project\Tools\Utility\Content' => $classPath . 'utility/content.php',
    'Project\Tools\Utility\Cron' => $classPath . 'utility/cron.php',
    'Project\Tools\Utility\Curl' => $classPath . 'utility/curl.php',
    'Project\Tools\Utility\Dir' => $classPath . 'utility/dir.php',
    'Project\Tools\Utility\Lang' => $classPath . 'utility/lang.php',
    'Project\Tools\Utility\Request' => $classPath . 'utility/request.php',
    'Project\Tools\Utility\Settings' => $classPath . 'utility/settings.php',
    'Project\Tools\Utility\Sort' => $classPath . 'utility/sort.php',
    'Project\Tools\Utility\Tag' => $classPath . 'utility/tag.php',
    'Project\Tools\Utility\User' => $classPath . 'utility/user.php',
]);
