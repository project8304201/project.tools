<?php

namespace Project\Tools\Sale;

use CCatalogDiscount,
    Bitrix\Main\Loader,
    Project\Tools\Utility\Cache;

class Discount {

    static private function getProductsList() {
        return Cache::getCached(array(__CLASS__, __FUNCTION__), function() {
                    $arDiscountElementID = array();
                    if (!Loader::includeModule('catalog')) {
                        return $arDiscountElementID;
                    }
                    global $DB;
                    $dbProductDiscounts = CCatalogDiscount::GetList(
                                    array("SORT" => "ASC"), array(
                                "ACTIVE" => "Y",
                                "!>ACTIVE_FROM" => $DB->FormatDate(date("Y-m-d H:i:s"), "YYYY-MM-DD HH:MI:SS", CSite::GetDateFormat("FULL")),
                                "!<ACTIVE_TO" => $DB->FormatDate(date("Y-m-d H:i:s"), "YYYY-MM-DD HH:MI:SS", CSite::GetDateFormat("FULL")),
                                    ), false, false, array(
                                "ID", "SITE_ID", "ACTIVE", "ACTIVE_FROM", "ACTIVE_TO",
                                "RENEWAL", "NAME", "SORT", "MAX_DISCOUNT", "VALUE_TYPE",
                                "VALUE", "CURRENCY", "PRODUCT_ID"
                                    )
                    );
                    while ($arProductDiscounts = $dbProductDiscounts->Fetch()) {
                        if ($res = CCatalogDiscount::GetDiscountProductsList(array(), array(">=DISCOUNT_ID" => $arProductDiscounts['ID']), false, false, array())) {
                            while ($ob = $res->Fetch()) {
                                if (!in_array($ob["PRODUCT_ID"], $arDiscountElementID))
                                    $arDiscountElementID[] = $ob["PRODUCT_ID"];
                            }
                        }
                    }
                    return $arDiscountElementID;
                });
    }

}
