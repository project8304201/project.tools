<?php

namespace Project\Tools\Update;

use CCatalogProduct,
    CCatalogSKU,
    CPrice,
    Project\Tools\Config;

Catalog::setSelect(array(
    'ID',
    'IBLOCK_ID',
    'ACTIVE',
    'NAME',
    'DETAIL_PAGE_URL',
    'DETAIL_PICTURE',
    'PROPERTY_MORE_PHOTO',
    'PROPERTY_OLD_NID',
    'PROPERTY_OLD_VID',
    'CATALOG_GROUP_' . Config::priceId(),
));

class Catalog extends Iblock {

    static private function catalogPrice() {
        return 'CATALOG_PRICE_' . Config::catalogPriceId();
    }

    static private function catalogPriceId() {
        return 'CATALOG_PRICE_ID_' . Config::catalogPriceId();
    }

    static public function saveCatalog($arItem, $isOffers = false, $quality = null, $pricePurchase = null, $pricePurchaseId = null) {
        $ID = $arItem['ID'];
        $arFields = array();
        $type = $isOffers ? CCatalogSKU::TYPE_PRODUCT : CCatalogSKU::TYPE_FULL;
        if (is_null($arItem['CATALOG_QUANTITY']) or ( !is_null($quality) and $quality != $arItem['CATALOG_QUANTITY'])) {
            $arFields["QUANTITY"] = (int) $quality;
        }
        if ('Y' != $arItem['CATALOG_AVAILABLE']) {
            $arFields["CATALOG_TYPE"] = $isOffers ? CCatalogSKU::TYPE_PRODUCT : CCatalogSKU::TYPE_FULL;
            $arFields["CATALOG_AVAILABLE"] = 'Y';
        }
        if (!is_null($pricePurchase)) {
            if ($pricePurchase != $arItem['CATALOG_PURCHASING_PRICE']) {
                $arFields["PURCHASING_PRICE"] = $pricePurchase;
                $arFields["PURCHASING_CURRENCY"] = $pricePurchaseId;
            }
        }
//        pre($arFields);
        if ($arFields) {
            if ($arItem[self::CATALOG_PRICE_ID]) {
                CCatalogProduct::Update($ID, $arFields);
            } else {
                $arFields['ID'] = $ID;
                //            pre($quality);
                CCatalogProduct::Add($arFields);
            }
        }
    }

    static public function savePrice($arItem, $price, $priceId) {
        if ($priceId != 'RUB') {
//            preExit($arItem['ID'], $priceId);
        }
        $arFields = array();
        if ($price != $arItem[self::CATALOG_PRICE]) {
            $arFields["PRICE"] = $price;
            $arFields["CURRENCY"] = $priceId;
        }
//        pre($arFields);
        if ($arFields) {
            $arFields['PRODUCT_ID'] = $arItem['ID'];
            if ($arItem[self::CATALOG_PRICE_ID]) {
//                pre($arFields);
                CPrice::Update($arItem[self::CATALOG_PRICE_ID], $arFields);
            } else {
                $arFields['CATALOG_GROUP_ID'] = Config::priceId();
//                pre($arFields);
                CPrice::Add($arFields);
            }
        }
    }

}
