<?php

namespace Project\Tools;

use Bitrix\Main\Config\Option;

class Config
{

    static private $arParams = [
        /* Отладка */
        'name'                 => '_',
        /* Отладка */
        'adminDebug'           => [1],
        /* Кеширование */
        'isCacheDebug'         => false,
        'isCache'              => true,
        /* Каталог */
        'catalogPriceId'       => 1,
        /* ватермарки */
        'isWarermark'          => true,
        'warermarkPath'        => '/images/warermark.png',
        'warermarkСoefficient' => 0.8,

        /* cron */
        'cronTimeOut' => 60 * 5,
    ];

    /**
     * @param $arParams
     */
    static public function set($arParams)
    {
        self::$arParams = array_merge(self::$arParams, $arParams);
    }

    /**
     * @param $key
     * @param $value
     */
    static public function setParams($key, $value)
    {
        self::$arParams[$key] = $value;
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return bool
     */
    static public function __callStatic($name, $arguments)
    {
        return self::$arParams[$name] ?: false;
    }

    /**
     * @return array|null
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    static public function adminDebug()
    {
        static $arUser = null;
        if (is_null($arUser)) {
            if ($adminDebug = Option::get(self::name(), 'adminDebug')) {
                $arUser = explode(',', $adminDebug);
            } else {
                $arUser = [];
            }
        }
        return $arUser;
    }
}
