<?

namespace Project\Tools\Model;

use Bitrix\Main\Application;
use Project\Tools\Trains\Event;

class Data
{
    const FILE = '/upload/tmp/';
    private string $file = '';

    use Event;

    /**
     * @param string $file
     */
    function __construct(string $file)
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return Application::getDocumentRoot() . self::FILE . $this->file;
    }

    /**
     * @return array
     */
    public function &get(): array
    {
        static $arData = false;
        if ($arData === false) {
            $arData = [];
            if (is_file($this->getPath())) {
                $content = file_get_contents($this->getPath());
                try {
                    $arData = unserialize($content);
                } catch (Exception $e) {
                }
            }
        }
        return $arData;
    }

    /**
     *
     */
    public function clear()
    {
        $arData = &$this->get();
        $arData['START'] = time();
        unset($arData['DATA']);
        $this->save();
    }

    /**
     * @param string $type
     * @param string $ID
     *
     * @return $this
     */
    public function add(string $type, string $ID)
    {
        $this->get()['DATA'][$type] = $ID;
        return $this;
    }

    public function delete(string $type)
    {
        unset($this->get()['DATA'][$type]);
        return $this;
    }

    /**
     *
     */
    public function save()
    {
//        pre($this->getPath(), serialize($this->get()));
        file_put_contents($this->getPath(), serialize($this->get()));
    }

}
