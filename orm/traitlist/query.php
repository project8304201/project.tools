<?php

namespace Project\Tools\Orm\TraitList;

use Project\Tools\Orm\Entity;

trait Query {

    public static function queryCustom()
    {
        return new Entity\Query(static::getEntity());
    }

}