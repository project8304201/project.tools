<?php

namespace Project\Tools\Orm\TraitList;

use Bitrix\Main\Entity\ScalarField;
use Bitrix\Main\ORM\Data\AddResult;

trait BigData
{

    static private $isBigData = false;
    static private $arInsert = [];

    /**
     *
     */
    public static function startBigData()
    {
        self::$isBigData = true;
    }

    /**
     *
     */
    public static function compileBigData()
    {
        self::$isBigData = false;
        if (empty(self::$arInsert)) {
            return;
        }
        $entity = static::getEntity();
        foreach ($entity->getFields() as $field) {
            if ($field instanceof ScalarField) {
                $defaultValue = $field->getDefaultValue();
                if ($defaultValue !== null) {
                    foreach (self::$arInsert as &$arItem) {
                        if (!array_key_exists($field->getName(), $arItem)) {
                            $arItem[$field->getName()] = $field->getDefaultValue();
                        }
                    }
                }
            }
        }

        $connection = $entity->getConnection();
        $tableName = $entity->getDBTableName();

        $sql = '';
        foreach (self::$arInsert as &$arItem) {
            if(!isset($arItem['ROWS_ID']) || $arItem['ROWS_ID'] > 0){
                foreach ($arItem as $fieldName => $value) {
                    $field = static::getEntity()->getField($fieldName);
                    $arItem[$fieldName] = $field->modifyValueBeforeSave($value, $arItem);
                }
                $dataReplacedColumn = static::replaceFieldName($arItem);
                $insert = $connection->getSqlHelper()->prepareInsert($tableName, $dataReplacedColumn);
                if (empty($sql)) {
                    $sql =
                        "REPLACE INTO " . $tableName . "(" . $insert[0] . ") " .
                        "VALUES (" . $insert[1] . ")";
                } else {
                    $sql .= ", (" . $insert[1] . ")";
                }
            }
        }

        self::$arInsert = [];
        if ($sql) {
            $connection->queryExecute($sql);
        }
    }

    /**
     * @param array $data
     *
     * @return AddResult
     */
    public static function add(array $data)
    {
        if (self::$isBigData) {
            return self::addBigData($data);
        } else {
            return parent::add($data);
        }
    }

    /**
     * @param array $data
     *
     * @return AddResult
     */
    public static function addBigData(array $data)
    {
        if (count(self::$arInsert) > static::BIGDATA_LIMIT) {
            self::compileBigData();
            self::startBigData();
        }
        self::$arInsert[] = $data;
        return new AddResult();
    }

}