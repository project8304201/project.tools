<?php

namespace Project\Tools\Orm\Entity;

use Bitrix\Main;

class Query extends Main\Entity\Query
{

    /**
     * @param $where
     *
     * @return Main\DB\ArrayResult|Main\DB\Result
     * @throws Main\ArgumentException
     * @throws Main\Db\SqlQueryException
     * @throws Main\SystemException
     */
    public function deleteByWhere($where)
    {
        $this->is_executing = true;
        $helper = static::getEntity()->getConnection()->getSqlHelper();

        $this->setFilter($where);
        $this->buildQuery();

        $query = "DELETE FROM " . $this->quoteTableSource($this->entity->getDBTableName()) . " WHERE " . str_replace($helper->quote($this->getInitAlias()) . '.',
                '', parent::buildWhere());
        $result = $this->query($query);
        $this->is_executing = false;
        return $result;
    }

    /**
     * @return mixed
     * @throws Main\SystemException
     */
    protected function buildGroup()
    {
        foreach ($this->global_chains as $key => $chain) {
            if (!in_array($chain->getAlias(), $this->group)) {
                unset($this->global_chains[$key]);
            }
        }
        return parent::buildGroup();
    }

}
