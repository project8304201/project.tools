<?

namespace Project\Tools\Trains;

use Exception;

trait Event
{

    static private $isStart = [];

    /**
     * @throws Exception
     */
    static protected function evetType()
    {
        throw new Exception('Установите тип события');
    }

    /**
     * @return bool
     * @throws Exception
     */
    static protected function start()
    {
        if (empty(self::$isStart[static::evetType()])) {
            self::$isStart[static::evetType()] = true;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    static protected function stop()
    {
        unset(self::$isStart[static::evetType()]);
        return false;
    }

}
