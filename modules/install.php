<?php

namespace Project\Tools\Modules;

include_once(__DIR__ . '/utility.php');

const IS_START = true;

use Bitrix\Main\Application;
use Bitrix\Main\EventManager;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Exception;
use Project\Tools\Utility\Agent;
use Project\Tools\Utility\Lang;

Lang::load(__FILE__);

trait Install
{

    private $dir = '';
    private $message = '';

    protected function setParam($dir, $message)
    {
        $this->dir = $dir;
        $this->message = $message;
        include($this->dir . '/version.php');
        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }
    }

    /* message */

    public function getMessage($message, $data = [])
    {
        return Loc::getMessage($this->message . '_' . $message, $data) ?: Loc::getMessage('Project\Tools_' . $message,
            $data);
    }

    public function Install()
    {
        Application::getConnection()->startTransaction();
        ModuleManager::registerModule($this->MODULE_ID);
        Loader::includeModule($this->MODULE_ID);
        try {
            $this->InstallDB();
        } catch (Exception $e) {
            Application::getConnection()->rollbackTransaction();
            ModuleManager::unRegisterModule($this->MODULE_ID);
            return Utility::ThrowException($this->getMessage('INSTALL_DB_ERROR', [
                'ERROR' => $e->getMessage(),
            ]));
        }
        try {
            $this->InstallFiles();
        } catch (Exception $e) {
            Application::getConnection()->rollbackTransaction();
            ModuleManager::unRegisterModule($this->MODULE_ID);
            return Utility::ThrowException($this->getMessage('INSTALL_FILES_ERROR', [
                'ERROR' => $e->getMessage(),
            ]));
        }
        try {
            $this->InstallGadget();
        } catch (Exception $e) {
            Application::getConnection()->rollbackTransaction();
            ModuleManager::unRegisterModule($this->MODULE_ID);
            return Utility::ThrowException($this->getMessage('INSTALL_GADGET_ERROR', [
                'ERROR' => $e->getMessage(),
            ]));
        }
        $this->InstallEvent();
        $this->InstallAgent();
        Application::getConnection()->commitTransaction();
    }

    public function Uninstall()
    {
        Application::getConnection()->startTransaction();
        Loader::includeModule($this->MODULE_ID);
        try {
            $this->UnInstallDB();
        } catch (Exception $e) {
            Application::getConnection()->rollbackTransaction();
            return Utility::ThrowException($this->getMessage('UNINSTALL_DB_ERROR', [
                'ERROR' => $e->getMessage(),
            ]));
        }
        try {
            $this->UnInstallFiles();
        } catch (Exception $e) {
            Application::getConnection()->rollbackTransaction();
            return Utility::ThrowException($this->getMessage('UNINSTALL_FILES_ERROR', [
                'ERROR' => $e->getMessage(),
            ]));
        }
        try {
            $this->UnInstallGadget();
        } catch (Exception $e) {
            Application::getConnection()->rollbackTransaction();
            return Utility::ThrowException($this->getMessage('UNINSTALL_GADGET_ERROR', [
                'ERROR' => $e->getMessage(),
            ]));
        }
        $this->UnInstallEvent();
        $this->UnInstallAgent();
        ModuleManager::unRegisterModule($this->MODULE_ID);
        Application::getConnection()->commitTransaction();
    }

    /*
     * InstallEvent
     */

    public function registerEventHandler($modul, $event, $class, $func)
    {
        EventManager::getInstance()->registerEventHandler($modul, $event, $this->MODULE_ID, $class, $func);
    }

    public function unRegisterEventHandler($modul, $event, $class, $func)
    {
        EventManager::getInstance()->unRegisterEventHandler($modul, $event, $this->MODULE_ID, $class, $func);
    }

    public function InstallEvent()
    {

    }

    public function UnInstallEvent()
    {

    }

    /*
     * InstallDB
     */

    public function InstallDB()
    {

    }

    public function UnInstallDB()
    {

    }

    /*
     * InstallFiles
     */

    public function InstallFiles()
    {

    }

    public function UnInstallFiles()
    {

    }

    /*
     * InstallGadget
     */

    public function InstallGadget()
    {

    }

    public function UnInstallGadget()
    {

    }

    /*
     * InstallAgent
     */

    public function InstallAgent()
    {
//        Project\Tools\Utility\Agent::add(
//                '\Tm\Portal\Agent\Tasks::process();', // имя функции
//                $this->MODULE_ID, // идентификатор модуля
//                "N", // агент не критичен к кол-ву запусков
//                30 * 60, // интервал запуска - 1 сутки
//                "", // дата первой проверки - текущее
//                "N", // агент активен
//                "", // дата первого запуска - текущее
//                30);
    }

    public function UnInstallAgent()
    {
//        CAgent::RemoveAgent('\Tm\Portal\Agent\Tasks::process();', $this->MODULE_ID);
    }

    /**
     * @param        $name
     * @param string $period
     * @param int    $interval
     * @param string $datecheck
     * @param string $active
     * @param string $next_exec
     * @param int    $sort
     * @param bool   $user_id
     * @param bool   $existError
     */
    public function AddAgent(
        $name, // PHP function name
        $period = "N", // check for agent execution count in period of time
        $interval = 86400, // time interval between execution
        $datecheck = "", // first check for execution time
        $active = "Y", // is the agent active or not
        $next_exec = "", // first execution time
        $sort = 100, // order
        $user_id = false, // user
        $existError = true // return error, if agent already exist
    )
    {
        Agent::add(
            $name, $this->MODULE_ID, $period, $interval, $datecheck, $active, $next_exec, $sort, $user_id, $existError
        );
//            pre(
//                    $name, $module, $period, $interval, $datecheck, $active, $next_exec, $sort, $user_id, $existError
//            );
    }

}
