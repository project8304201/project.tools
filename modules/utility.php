<?php

namespace Project\Tools\Modules;

use CEventLog;
use Exception,
    Bitrix\Main\Application,
    Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\ModuleManager;
use Project\Tools\Config;

class Utility
{
    /* db */

    public static function createDbTable($table)
    {
        if (!Application::getInstance()->getConnection()->isTableExists($table::getTableName())) {
            $table::getEntity()->createDbTable();
        }
    }

    public static function dropTable($table)
    {
        Application::getInstance()->getConnection()->dropTable($table::getTableName());
    }

    public function RunSqlBatch($file)
    {
        global $DB;
        $DB->RunSQLBatch($this->dir . '/' . $file);
    }

    /* message */

//    public static function getMessage($message, $data = array())
//    {
//        return Loc::getMessage($this->message . '_' . $message, $data);
//    }

    /* exception */

    public static function ThrowException($message, $data = array())
    {
        global $APPLICATION;
        $APPLICATION->ResetException();
        $APPLICATION->ThrowException($message, $data);
        CEventLog::Add(array(
            "SEVERITY" => "ERROR",
            "AUDIT_TYPE_ID" => "INSTALL_ERROR",
            "MODULE_ID" => Config::name(),
            "DESCRIPTION" => $message,
        ));
//        preError($message, $data);
        preExit($message, $data);
    }

}
