<?php

$MESS['Project\Tools_INSTALL_DB_ERROR'] = 'Произошла ошибка в процессе инсталляции БД модуля. ERROR';
$MESS['Project\Tools_UNINSTALL_DB_ERROR'] = 'Произошла ошибка в процессе деинсталляции БД модуля. ERROR';
$MESS['Project\Tools_INSTALL_FILES_ERROR'] = 'Произошла ошибка в процессе инсталляции файлов модуля. ERROR';
$MESS['Project\Tools_UNINSTALL_FILES_ERROR'] = 'Произошла ошибка в процессе деинсталляции файлов модуля. ERROR';
$MESS['Project\Tools_INSTALL_REQUIRE_MODULES_ERROR'] = 'Ниобходим модуль #MODULE#';
$MESS['Project\Tools_INSTALL_MIN_MAIN_VERSION_ERROR'] = 'Минимальная версия bitrix #VERSION#';
