<?php

$MESS['Project\Tools_INSTALL_DB_ERROR'] = 'An error occurs during database installation. ERROR';
$MESS['Project\Tools_UNINSTALL_DB_ERROR'] = 'An error occurs during database uninstallation. ERROR';
$MESS['Project\Tools_INSTALL_FILES_ERROR'] = 'An error occurs during midule files installation. ERROR';
$MESS['Project\Tools_UNINSTALL_FILES_ERROR'] = 'An error occurs during midule files uninstallation. ERROR';
