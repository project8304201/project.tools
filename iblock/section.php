<?php

namespace Project\Tools\Iblock;

use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Loader;
use CFile;
use CIBlockSection;
use Exception;
use Project\Tools\Utility\Cache;
use Project\Tools\Utility\Image;

class Section
{

    /**
     * Получает ИД свойства по коду
     *
     * @param      $ID
     * @param bool $useCache
     *
     * @return int
     * @throws Exception
     */
    public static function getById($ID, $useCache = true)
    {
        return Cache::getCached(function () use ($ID) {
            Loader::includeModule('iblock');
            $res = CIBlockSection::GetByID($ID);
            if ($arSection = $res->GetNext()) {
                return $arSection;
            }
            return 0;
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => $ID,
            'MEMORY'     => true,
            'TAGS'       => Tag::section($ID),
        ], !$useCache);
    }

    /**
     * @param      $arFilter
     * @param bool $useCache
     *
     * @return null
     * @throws Exception
     */
    public static function getIdByFilter($arFilter, $useCache = true)
    {
        return Cache::getCached(function () use ($arFilter) {
            Loader::includeModule('iblock');
            $res = CIBlockSection::GetList([], $arFilter, false, ['ID']);
            if ($arSection = $res->Fetch()) {
                Tag::register(Tag::section($arSection['ID']));
                return $arSection['ID'];
            }
            return 0;
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => $arFilter,
            'MEMORY'     => true,
        ], !$useCache);
    }

    /**
     * @param       $arFilter
     * @param array $arSelect
     * @param bool  $useCache
     *
     * @return null
     * @throws Exception
     */
    public static function getByFilter($arFilter, $arSelect = ['ID'], $useCache = true)
    {
        return Cache::getCached(function () use ($arFilter, $arSelect) {
            Loader::includeModule('iblock');
            $res = CIBlockSection::GetList([], $arFilter, false, $arSelect);
            if ($arSection = $res->Fetch()) {
                Tag::register(Tag::section($arSection['ID']));
                return $arSection;
            }
            return 0;
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => [$arFilter, $arSelect],
            'MEMORY'     => true,
        ], !$useCache);
    }


    public static function getUFById($arFilter, $UF, $useCache = true)
    {
        return Cache::getCached(function () use ($arFilter, $UF) {
            Loader::includeModule('iblock');
            $res = CIBlockSection::GetList([], $arFilter, false, ['ID', $UF]);
            if ($arSection = $res->GetNext()) {
                Tag::register(Tag::section($arSection['ID']));
                return $arSection['~' . $UF];
            }
            return '';
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => [$arFilter, $UF],
        ], !$useCache);
    }

    /**
     * @param array $arOrder
     * @param array $arFilter
     * @param bool  $bIncCnt
     * @param array $arSelect
     * @param bool  $arNavStartParams
     *
     * @return null
     * @throws Exception
     */
    public static function getList(
        $arOrder = ["SORT" => "ASC"],
        $arFilter = [],
        $bIncCnt = false,
        $arSelect = [],
        $arNavStartParams = false
    ) {
        return Cache::getCached(function () use ($arOrder, $arFilter, $bIncCnt, $arSelect, $arNavStartParams) {
            $arSelect[] = 'ID';
            $db_list = CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $arSelect, $arNavStartParams);
            $arResult = [];
            while ($ar_result = $db_list->GetNext()) {
                Tag::register(Tag::section($ar_result['ID']));
                $arResult[$ar_result["ID"]] = $ar_result;
            }
            return $arResult;
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
//            'PARAM'      => [$allSections, $iblickId],
            'PARAM'      => [$arOrder, $arFilter, $bIncCnt, $arSelect, $arNavStartParams],
        ]);
    }

    /**
     * @param $arFilter
     *
     * @return null
     * @throws Exception
     */
    public static function getListCode($arFilter)
    {
        return Cache::getCached(function () use ($arFilter) {
            $db_list = CIBlockSection::GetList([], $arFilter, false, ['ID', 'IBLOCK_SECTION_ID', 'NAME', 'CODE']);
            $arResult = [];
            while ($ar_result = $db_list->Fetch()) {
                Tag::register(Tag::section($ar_result['ID']));
                $arResult[$ar_result['IBLOCK_SECTION_ID']][$ar_result['ID']] = [
                    "NAME" => $ar_result["NAME"],
                    "CODE" => $ar_result["CODE"],
                ];
            }
            return $arResult;
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => $arFilter,
        ]);
    }

    /**
     * @param      $arSort
     * @param      $arFilter
     * @param      $arSelect
     * @param bool $bIncCnt
     *
     * @return null
     * @throws Exception
     */
    public static function getListAray($arSort, $arFilter, $arSelect, $bIncCnt = false, $useCache = true)
    {
        return Cache::getCached(function () use ($arSort, $arFilter, $arSelect, $bIncCnt) {
            $arSelect[] = 'ID';
            $res = CIBlockSection::GetList($arSort, $arFilter, $bIncCnt, $arSelect);
            $arResult = [];
            while ($arItem = $res->GetNext()) {
                Tag::register(Tag::section($arItem['ID']));
                $arResult[] = $arItem;
            }
            return $arResult;
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => [$arSort, $arFilter, $arSelect, $bIncCnt],
        ], !$useCache);
    }

    /**
     * @param $allSections
     * @param $arSelect
     * @param $boolPicture
     * @param $boolDescr
     *
     * @return null
     * @throws Exception
     */
    public static function getListPicture($allSections, $arSelect, $boolPicture, $boolDescr)
    {
        return Cache::getCached(function () use ($allSections, $arSelect, $boolPicture, $boolDescr) {
            $arSelect[] = 'ID';
            $rsSections = CIBlockSection::GetList([], ['ID' => $allSections], false, $arSelect);
            $arResult = [];
            while ($arSection = $rsSections->GetNext()) {
                Tag::register(Tag::section($arSection['ID']));
                $arItem = [
                    'ID' => $arSection['ID'],
                ];
                if ($boolPicture) {
                    $arItem['PICTURE'] = $arSection['PICTURE'] ? CFile::GetFileArray($arSection['PICTURE']) : false;
                    $arItem['~PICTURE'] = $arSection['~PICTURE'];
                }
                if ($boolDescr) {
                    $arItem['DESCRIPTION'] = $arSection['DESCRIPTION'];
                    $arItem['~DESCRIPTION'] = $arSection['~DESCRIPTION'];
                    $arItem['DESCRIPTION_TYPE'] = $arSection['DESCRIPTION_TYPE'];
                    $arItem['~DESCRIPTION_TYPE'] = $arSection['~DESCRIPTION_TYPE'];
                }
                $arResult[] = $arItem;
            }
            return $arResult;
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => [$allSections, $arSelect, $boolPicture, $boolDescr],
        ]);
    }

    /**
     * @param $ID
     * @param $width
     * @param $height
     *
     * @return null
     * @throws \Exception
     */
    static function getPictureByID($ID, $width, $height)
    {
        return Cache::getCached(function () use ($ID, $width, $height) {
            $arItem = SectionTable::getList([
                'select' => ['PICTURE'],
                'filter' => [
                    'ID' => $ID,
                ],
            ])->Fetch();
            if ($arItem && $arItem['PICTURE']) {
                $arImg = CFile::GetFileArray($arItem["PICTURE"]);
                $arImg['SRC'] = Image::resize($arImg['ID'], $width, $height);
                return $arImg;
            }
            return 0;
        }, [
            'PARAM' => [$ID, $width, $height],
            'TAGS'  => Tag::section($ID),
        ]);
    }

    /**
     * @param $iblickId
     * @param $tempA
     *
     * @return null
     * @throws Exception
     */
    public static function getTreeList($iblickId, $tempA)
    {
        $arResult = Cache::getCached(function () use ($iblickId) {
            $arResult = [];
            $arFilter = ['IBLOCK_ID' => $iblickId, 'GLOBAL_ACTIVE' => 'Y'];
            $db_list = CIBlockSection::GetTreeList($arFilter);
            while ($ar_result = $db_list->GetNext()) {
                Tag::register(Tag::section($ar_result['ID']));
                $arResult[] = [
                    "ID"          => $ar_result['ID'],
                    "NAME"        => $ar_result['NAME'],
                    "DEPTH_LEVEL" => (int)$ar_result['DEPTH_LEVEL'],
                ];
            }
            return $arResult;
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => $iblickId,

        ]);

        foreach ($arResult as &$ar_result) {
            if (in_array($ar_result["ID"], $tempA)) {
                $ar_result["CHECKED"] = 1;
            } else {
                $ar_result["CHECKED"] = 0;
            }
        }
        return $arResult;
    }

    public static function getNavChain($IBLOCK_ID, $SECTION_ID, $arSelect = [], $arrayResult = false)
    {
        return Cache::getCached(function () use ($IBLOCK_ID, $SECTION_ID, $arSelect, $arrayResult) {
            return CIBlockSection::GetNavChain($IBLOCK_ID, $SECTION_ID, $arSelect, $arrayResult)->Fetch() ?: 0;
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => [$IBLOCK_ID, $SECTION_ID, $arSelect, $arrayResult],
            'TAGS'       => Tag::section($SECTION_ID),
        ]);
    }


    /**
     * @param      $ID
     * @param bool $useCache
     *
     * @return int
     * @throws Exception
     */
    public static function getFiledsById($ID, $arFileds, $useCache = true)
    {
        return Cache::getCached(function () use ($ID, $arFileds) {
            Loader::includeModule('iblock');
            $arResSection = CIBlockSection::GetByID($ID);
            if ($arSect = $arResSection->GetNext()) {
                $arResult = [];
                foreach ($arFileds as $code) {
                    $arResult[$code] = $arSect[$code];
                }
                return $arResult;
            }
            return 0;
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => [$ID, $arFileds],
            'TAGS'       => Tag::section($ID),
        ], !$useCache);
    }

    public static function getStaticFieldsById($iblockId, $id, $code)
    {
        static $arResult = [];
        if (!isset($arResult[$iblockId][$code])) {
            $arResult[$iblockId][$code] = Cache::getCached(function () use ($iblockId, $code) {
                $arResult = [];
                $arFilter = ['IBLOCK_ID' => $iblockId];
                $res = CIBlockSection::GetList([], $arFilter, false, ['ID', 'IBLOCK_ID', $code]);
                while ($arItem = $res->Fetch()) {
                    Tag::register(Tag::section($arItem['ID']));
                    $arResult[$arItem['ID']] = $arItem[$code];
                }
                return $arResult;
            }, [
                'CACHE_TIME' => Cache::TIME_DAY,
                'PARAM'      => [$iblockId, $code],
            ]);
        }
        return $arResult[$iblockId][$code][$id] ?: false;
    }

    /**
     * @param       $arFilter
     * @param       $arSelect
     * @param array $arSort
     *
     * @return null
     * @throws \Exception
     */
    static function getFetchByFilter($arFilter, $arSelect, $arSort = [])
    {
        return Cache::getCached(function () use ($arFilter, $arSelect, $arSort) {
            $arSelect[] = 'ID';
            $resMeta = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
            if ($arItem = $resMeta->Fetch()) {
                Tag::register(Tag::section($arItem['ID']));
                return $arItem;
            } else {
                return 0;
            }
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => [$arFilter, $arSelect, $arSort],
        ]);
    }

}
