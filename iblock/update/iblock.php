<?php

namespace Project\Tools\Iblock\Update;

use CIBlockElement;
use Cutil;
use Exception;

class Iblock
{

    static private $arSelect = [
        'ID',
        'IBLOCK_ID',
        'ACTIVE',
        'NAME',
        'DETAIL_PAGE_URL',
        'DETAIL_PICTURE',
        'PROPERTY_MORE_PHOTO',
    ];

    /**
     * @param $str
     *
     * @return string
     */
    static public function translit($str)
    {
        return Cutil::translit($str, "ru", ["replace_space" => "-", "replace_other" => "-"]);
    }

    /**
     * @param $arSelect
     */
    static public function setSelect($arSelect)
    {
        self::$arSelect = $arSelect;
    }

    /**
     * @return array
     */
    static protected function getSelect()
    {
        return self::$arSelect;
    }

    /**
     * @param $arFields
     *
     * @return mixed
     */
    static public function add($arFields)
    {
        $el = new CIBlockElement;
        if (!$arFields['ID'] = $el->Add($arFields)) {
            preExit($el->LAST_ERROR);
        }
        return self::searchProductById($arFields);
    }

    /**
     * @param $arFields
     * @param $propFields
     *
     * @return mixed
     */
    static public function addItem($arFields, $propFields)
    {
        $el = new CIBlockElement;
        if (!$arFields['ID'] = $el->Add($arFields)) {
            preExit($el->LAST_ERROR);
        }
        foreach ($propFields as $key => $value) {
            CIBlockElement::SetPropertyValues($arFields['ID'], $arFields['IBLOCK_ID'], $value, $key);
        }
        return $arFields['ID'];
    }

    /**
     * @param $arItem
     * @param $arFields
     * @param $propFields
     */
    static public function update($arItem, $arFields, $propFields)
    {
        if ($arFields) {
            $el = new CIBlockElement;
            $el->Update($arItem['ID'], $arFields);
        }
        foreach ($propFields as $key => $value) {
            CIBlockElement::SetPropertyValues($arItem['ID'], $arItem['IBLOCK_ID'], $value, $key);
        }
    }

    /**
     * @param      $arFilter
     * @param      $arFields
     * @param      $propFields
     * @param bool $isUpdate
     *
     * @return mixed
     * @throws Exception
     */
    static public function searchByFilter($arFilter, $arFields, $propFields, $isUpdate = true)
    {
        $arItem = CIBlockElement::GetList([], $arFilter, false, false, static::$arSelect)->fetch();
        if ($arItem) {
            if ($isUpdate) {
                self::update($arItem, $arFields, $propFields);
            }
            return self::searchById($arItem);
        } else {
            $el = new CIBlockElement;
            if (!$arFields['ID'] = $el->Add($arFields)) {
                throw new Exception($el->LAST_ERROR);
            }
            foreach ($propFields as $key => $value) {
                CIBlockElement::SetPropertyValues($arFields['ID'], $arFields['IBLOCK_ID'], $value, $key);
            }
            return self::searchById($arFields);
        }
    }

    /**
     * @param $arFields
     * @param $propFields
     *
     * @return mixed
     * @throws Exception
     */
    static public function searchByName($arFields, $propFields)
    {
        $arFilter = ["IBLOCK_ID" => $arFields['IBLOCK_ID'], 'NAME' => $arFields['NAME']];
        return self::searchByFilter($arFilter, $arFields, $propFields);
    }

    /**
     * @param $arItem
     *
     * @return mixed
     */
    static public function searchById($arItem)
    {
        $arFilter = [
            "IBLOCK_ID" => $arItem['IBLOCK_ID'],
            'ID'        => $arItem['ID'],
        ];
        return CIBlockElement::GetList([], $arFilter, false, false, self::getSelect())->Fetch();
    }

}
