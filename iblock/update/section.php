<?php

namespace Project\Tools\Iblock\Update;

use Cutil,
    CIBlockSection,
    Project\Tools\Utility;

class Section {

    static public function getByNameTree($iblockId, $section, $subsection = '', $subsection2 = '') {
        static $arSection = array();
        static $arSubSection = array();
        static $arSub2Section = array();

        if (empty($arSection)) {
            $tree = CIBlockSection::GetTreeList(
                            array('IBLOCK_ID' => $iblockId), array('ID', 'CODE', 'IBLOCK_SECTION_ID', 'DEPTH_LEVEL', 'NAME')
            );
            $parentList = array();
            while ($arItem = $tree->Fetch()) {
                switch ($arItem['DEPTH_LEVEL']) {
                    case 1:
                        $arSection[$iblockId][strtolower($arItem['NAME'])] = $arItem['ID'];
                        break;

                    case 2:
                        $arSubSection[$iblockId][$arItem['IBLOCK_SECTION_ID']][strtolower($arItem['NAME'])] = $arItem['ID'];
                        $parentList[$iblockId][$arItem['ID']] = $arItem['IBLOCK_SECTION_ID'];
                        break;

                    case 3:
                        $arSub2Section[$iblockId][$parentList[$arItem['IBLOCK_SECTION_ID']]][$arItem['IBLOCK_SECTION_ID']][strtolower($arItem['NAME'])] = $arItem['ID'];
                        break;

                    default:
                        break;
                }
            }
        }

        $sectionLower = strtolower($section);
        $subsectionLower = strtolower($subsection);
        $subsectionLower2 = strtolower($subsection2);

        $bs = new CIBlockSection;
        if (isset($arSection[$iblockId][$sectionLower])) {
            $sectionId = $arSection[$iblockId][$sectionLower];
        } else {
            $arFields = Array(
                "ACTIVE" => 'Y',
                "IBLOCK_SECTION_ID" => 0,
                "IBLOCK_ID" => $iblockId,
                "NAME" => $section,
                'CODE' => Utility\Content::translit($section),
                "SORT" => 500,
            );
            if (empty($arFields['CODE']) or $arFields['CODE'] == '-') {
                $arFields['CODE'] = Utility\Content::translate($arFields['NAME']);
            }
            $sectionId = $arSection[$iblockId][$sectionLower] = $bs->Add($arFields);
            if (empty($sectionId)) {
                pre($arFields);
                echo $bs->LAST_ERROR;
            }
        }

        if (empty($subsection)) {
            return $sectionId;
        }

        if (isset($arSubSection[$iblockId][$sectionId][$subsectionLower])) {
            $subsectionId = $arSubSection[$iblockId][$sectionId][$subsectionLower];
        } else {
            $arFields = Array(
                "ACTIVE" => 'Y',
                "IBLOCK_SECTION_ID" => $sectionId,
                "IBLOCK_ID" => $iblockId,
                "NAME" => $subsection,
                'CODE' => Utility\Content::translit($subsection),
                "SORT" => 500,
            );
            if (empty($arFields['CODE']) or $arFields['CODE'] == '-') {
                $arFields['CODE'] = Utility\Content::translate($arFields['NAME']);
            }
            $subsectionId = $arSubSection[$iblockId][$sectionId][$subsectionLower] = $bs->Add($arFields);
            if (empty($subsectionId)) {
                pre($arFields, $subsection, Utility\Content::translit($subsection));
                echo $bs->LAST_ERROR;
            }
        }


        if (empty($subsection2)) {
            return $subsectionId;
        }

        if (isset($arSub2Section[$iblockId][$sectionId][$subsectionId][$subsectionLower2])) {
            $sub2sectionId = $arSub2Section[$iblockId][$sectionId][$subsectionId][$subsectionLower2];
        } else {
            $arFields = Array(
                "ACTIVE" => 'Y',
                "IBLOCK_SECTION_ID" => $subsectionId,
                "IBLOCK_ID" => $iblockId,
                "NAME" => $subsection2,
                'CODE' => Utility\Content::translit($subsection2),
                "SORT" => 500,
            );
            if (empty($arFields['CODE']) or $arFields['CODE'] == '-') {
                $arFields['CODE'] = Utility\Content::translate($arFields['NAME']);
            }
            $sub2sectionId = $arSubSection[$iblockId][$sectionId][$subsectionId][$subsectionLower2] = $bs->Add($arFields);
            if (empty($subsectionId)) {
                echo $bs->LAST_ERROR;
            }
        }
        return $sub2sectionId;
    }

}
