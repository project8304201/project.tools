<?php

namespace Project\Tools\Iblock\Update;

use CIBlockPropertyEnum;

class Property
{

    static $arResplace = [
        'Зимняя' => 'Зимние',
        'Летняя' => 'Летние',
        'Зимняя' => 'Зимние',
        'Зимняя' => 'Зимние',
    ];

    /**
     * @param $arResplace
     */
    static public function setResplace($arResplace)
    {
        self::$arResplace = $arResplace;
    }

    /**
     * @param $iblock
     * @param $propertyId
     * @param $name
     *
     * @return mixed
     */
    public static function props($iblock, $propertyId, $name)
    {
        static $arProps = [];
        if (empty($arProps[$iblock][$propertyId])) {
            $arFilter = [
                'IBLOCK_ID'   => $iblock,
                'PROPERTY_ID' => $propertyId,
            ];
            $res = CIBlockPropertyEnum::GetList([], $arFilter);
            while ($arItem = $res->Fetch()) {
                $arProps[$iblock][$propertyId][$arItem['VALUE']] = $arItem['ID'];
            }
        }
        if (isset(self::$arResplace[$name])) {
            $name = self::$arResplace[$name];
        }
        if (empty($arProps[$iblock][$propertyId][$name])) {
            preExit($iblock, $propertyId, $name);
        }
        return $arProps[$iblock][$propertyId][$name];
    }

}
