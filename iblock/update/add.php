<?php

namespace Project\Tools\Iblock\Update;

use Bitrix\Main\Type\DateTime;
use CIBlock;
use CIBlockElement;
use CIBlockProperty;
use CIBlockType;
use CUserTypeEntity;
use Exception;

class Add
{

    static public function userField($arFields)
    {
        $res = CUserTypeEntity::GetList(
            array(),
            array(
                'ENTITY_ID' => $arFields['ENTITY_ID'],
                'FIELD_NAME' => $arFields['FIELD_NAME'],
            )
        );
        if (!$res->Fetch()) {
            $ob  = new CUserTypeEntity;
            $res = $ob->Add($arFields);
            if (empty($res)) {
                preExit($arFields, $ob->LAST_ERROR);
                throw new Exception($ob->LAST_ERROR);
            }
        }
    }

    static public function iblock($arSite, $arIblockType, $arIblock, $arProperties = [])
    {
        $res = CIBlockType::GetList(
            array(),
            array(
                '=ID' => $arIblockType['ID'],
            )
        );
        if (!$res->Fetch()) {
            $ib = new CIBlockType;
            $res = $ib->Add($arIblockType);
            if (empty($res)) {
                preExit($arIblockType, $ib->LAST_ERROR);
                throw new Exception($ib->LAST_ERROR);
            }
        }

        $res = CIBlock::GetList(
            array(),
            array(
                'TYPE' => $arIblock['TYPE'],
                'CODE' => $arIblock['CODE'],
            )
        );
        if ($arItem = $res->Fetch()) {
            $ID = $arItem['ID'];
        } else {
            $ib = new CIBlock;
            $ID = $ib->Add($arIblock);
            if (empty($ID)) {
                preExit($arIblock, $ib->LAST_ERROR);
                throw new Exception($ib->LAST_ERROR);
            }
            pre($ID, $ib->LAST_ERROR);
        }

        $ibp = new CIBlockProperty();
        foreach ($arProperties as $arProperty) {
            $arProperty['IBLOCK_ID'] = $ID;
            $res = CIBlockProperty::GetList(
                array(),
                array(
                    'IBLOCK_ID' => $arProperty['IBLOCK_ID'],
                    'CODE' => $arProperty['CODE'],
                )
            );
            if (!$res->Fetch()) {
                $res = $ibp->Add($arProperty);
                if (empty($res)) {
                    preExit($arProperty, $ibp->LAST_ERROR);
                    throw new Exception($ibp->LAST_ERROR);
                }
            }
        }
        return $ID;
    }

    static public function generateElement($arSite, $arIblockType, $arIblock, $arProperties = [])
    {
        $ID = self::iblock($arSite, $arIblockType, $arIblock, $arProperties);
        $res = CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID' => $ID),
            false,
            false,
            array('ID'),
        );
        if (!$res->Fetch()) {
            $arFields = array(
                'IBLOCK_ID' => $ID,
                'ACTIVE' => 'Y',
                'ACTIVE_FROM' => new DateTime(),
            );
            $el = new CIBlockElement;
            for ($i = 1; $i <= 15; $i++) {
                $arFields['NAME'] = time() . ' ' . $i;
                if (!$arFields['ID'] = $el->Add($arFields)) {
                    preExit($arFields, $el->LAST_ERROR);
                    throw new Exception($el->LAST_ERROR);
                }
            }
        }
    }

}
