<?php

namespace Project\Tools\Iblock;

use Bitrix\Main\Loader;
use CIBlockElement;
use CIBlockFormatProperties;
use CIBlockProperty;
use CIBlockPropertyEnum;
use Project\Tools\Utility\Cache;

class Property
{

    public static function getXmlIdByEnumId($iblockId, $propertyCode, $ID, $useCache = true)
    {
        static $arResult = [];
        if (empty($arResult)) {
            $arResult = Cache::getCached(function () use ($iblockId) {
                Loader::includeModule('iblock');
                $arResult = [];
                $db = CIBlockPropertyEnum::GetList(
                    [],
                    [
                        'IBLOCK_ID' => $iblockId,
                    ]
                );
                while ($arProperty = $db->Fetch()) {
                    $arResult[$iblockId][$arProperty['PROPERTY_CODE']][$arProperty['ID']] = $arProperty['XML_ID'];
                }
                return $arResult;
            }, [
                'PARAM' => $iblockId,
                'TAGS'  => Tag::iblock($iblockId),
            ], !$useCache);
        }
        return $arResult[$iblockId][$propertyCode][$ID] ?? false;
    }

    /**
     * Получает ИД свойства по коду
     *
     * @param        $iblockId
     * @param string $propertyCode Символьный код свойства
     * @param bool   $useCache
     *
     * @return int
     * @throws \Exception
     */
    public static function getIdByCode($iblockId, $propertyCode, $useCache = true)
    {
        return (int)Cache::getCached(function () use ($iblockId, $propertyCode) {
            Loader::includeModule('iblock');
            $db = CIBlockProperty::GetList(
                ['ID' => 'ASC'],
                [
                    'ACTIVE'    => 'Y',
                    'IBLOCK_ID' => $iblockId,
                    'CODE'      => $propertyCode,
                ]
            );
            if (!$property = $db->Fetch()) {
                throw new \Exception(sprintf('Property not found in iblock #%d!', $iblockId));
            }
            return $property['ID'];
        }, [
            'PARAM' => [$iblockId, $propertyCode],
            'TAGS'  => Tag::iblock($iblockId),
        ], !$useCache);
    }

    /**
     * @param $arFields
     * @param $propertyList
     */
    public static function getDisplayProperty(&$arFields, $propertyList)
    {
        $arElementLink = [];
        $arFields['PROPERTIES'] = [];
        $arElementLink[$arFields['ID']] = &$arFields['PROPERTIES'];
        $arPropFilter = [
            'ID'        => $arFields["ID"],
            'IBLOCK_ID' => $arFields['IBLOCK_ID'],
        ];
        CIBlockElement::GetPropertyValuesArray($arElementLink, $arFields["IBLOCK_ID"], $arPropFilter);
        foreach ($propertyList as $pid) {
            if (!isset($arFields["PROPERTIES"][$pid])) {
                continue;
            }
            $prop = &$arFields["PROPERTIES"][$pid];
            $boolArr = is_array($prop["VALUE"]);
            if (
                ($boolArr && !empty($prop["VALUE"]))
                || (!$boolArr && (string)$prop["VALUE"] !== '')
            ) {
                $arFields["DISPLAY_PROPERTIES"][$pid] = CIBlockFormatProperties::GetDisplayValue($arFields, $prop,
                    "catalog_out");
            }
            unset($prop);
        }
    }

    /**
     * @param      $arSort
     * @param      $arFilter
     * @param bool $useCache
     *
     * @return int
     * @throws \Exception
     */
    public static function getListNameCodeByFilter($arSort, $arFilter, $useCache = true)
    {
        return (int)Cache::getCached(function () use ($arSort, $arFilter) {
            Loader::includeModule('iblock');
            $res = CIBlockProperty::GetList($arSort, $arFilter);
            $arResult = [];
            while ($arProp = $res->Fetch()) {
                Tag::register(Tag::iblock($arProp['IBLOCK_ID']));
                if ($arProp['SORT'] >= 1000) {
                    $arResult[$arProp['NAME']] = $arProp['CODE'];
                }
            }
            return $arResult;
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => [$arSort, $arFilter],
        ], !$useCache);
    }

}
