<?php

namespace Project\Tools\Iblock;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use CIBlock;
use CIBlockProperty;
use CIBlockType;
use Project\Tools\Utility\Cache;

class Iblock
{

    static private function getConfig($type, $useCache = true)
    {
        static $iblocks = [];
        if (empty($iblocks)) {
            $iblocks = Cache::getCached(function () {
                $result = [];

                if (!Loader::includeModule('iblock')) {
                    throw new LoaderException('Module iblock not installed');
                }
                $db = CIBlock::GetList(
                    ['ID' => 'ASC'],
                    ['CHECK_PERMISSIONS' => 'N']
                );
                while ($item = $db->Fetch()) {
                    Tag::register(Tag::iblock($item['ID']));
                    if (!strlen($item['CODE'])) {
                        continue;
                    }
                    $result['OF_TYPE'][$item['IBLOCK_TYPE_ID']][$item['CODE']] = $item['ID'];
                    $result['OF_CODE'][$item['CODE']] = $item['ID'];
                    $result['OF_ID'][$item['ID']] = $item['CODE'];
                    $result['TYPE_OF_ID'][$item['ID']] = $item['IBLOCK_TYPE_ID'];
                    $result['TYPE_BY_ID'][$item['CODE']] = $item['IBLOCK_TYPE_ID'];
                }
                return $result;
            }, [], !$useCache);
        }
        return $iblocks[$type] ?? array();
    }

    /**
     * Возвращает ИД ИБ по его коду
     *
     * @param string $type  Тип ИБ
     * @param string $code  Код ИБ
     * @param bool   $cache Использовать или нет кэширование
     *
     * @return integer
     */
    static public function getByTypeCode($type, $code, $cache = true)
    {
        $iblocks = self::getConfig('OF_TYPE');
        return $iblocks[$type][$code] ?? '';
    }

    /**
     * @param      $code
     * @param bool $cache
     *
     * @return string
     */
    static public function getByCode($code, $cache = true)
    {
        $iblocks = self::getConfig('OF_CODE');
        return $iblocks[$code] ?? '';
    }

    /**
     * @param      $ID
     * @param bool $cache
     *
     * @return mixed|string
     */
    static public function getCodeById($ID, $cache = true)
    {
        $iblocks = self::getConfig('OF_ID');
        return $iblocks[$ID] ?? '';
    }

    /**
     * @param      $ID
     * @param bool $cache
     *
     * @return mixed|string
     */
    static public function getTypeById($ID, $cache = true)
    {
        $iblocks = self::getConfig('TYPE_OF_ID');
        return $iblocks[$ID] ?? '';
    }

    /**
     * @param      $code
     * @param bool $cache
     *
     * @return mixed|string
     */
    static public function getTypeByCode($code, $cache = true)
    {
        $iblocks = self::getConfig('TYPE_OF_CODE');
        return $iblocks[$code] ?? '';
    }

}
