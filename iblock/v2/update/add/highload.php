<?php

namespace Project\Tools\Iblock\V2\Update\Add;

use Project\Tools\Iblock\V2\Update;
use Bitrix\Highloadblock\HighloadBlockLangTable;
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Loader;
use Exception;

class Highload
{

    static public function add($arTable, $userFields, $ufName = 'UF_NAME', $arData = array())
    {
        Loader::includeModule('highloadblock');
        $res = HighloadBlockTable::GetList(
            array(
                'filter' => array(
                    '=TABLE_NAME' => $arTable['DATA']['TABLE_NAME'],
                ),
                'select' => array('ID'),
            )
        );
        if ($arItem = $res->Fetch()) {
            $ID = $arItem['ID'];
        } else {
            $result = HighloadBlockTable::add($arTable['DATA']);
            if ($ID = $result->getId()) {
                foreach ($arTable['LANG'] as $arLang) {
                    $arLang['ID'] = $ID;
                    HighloadBlockLangTable::add($arLang);
                }
            } else {
                throw new Exception(implode('<br>', $result->getErrorMessages()));
            }
        }
        foreach ($userFields as &$arFields) {
            $arFields['ENTITY_ID'] = 'HLBLOCK_' . $ID;
        }
        UserField::add($userFields);
        pre($arTable['DATA']['NAME'], $ufName, $arData);
        Update\Highload::add($arTable['DATA']['NAME'], $ufName, $arData);
    }

}
