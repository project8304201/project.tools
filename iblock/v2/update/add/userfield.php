<?php

namespace Project\Tools\Iblock\V2\Update\Add;

use CUserTypeEntity;
use Exception;

class UserField
{

    static public function add($userFields)
    {
        foreach ($userFields as $arFields) {
            $res = CUserTypeEntity::GetList(
                array(),
                array(
                    'ENTITY_ID' => $arFields['ENTITY_ID'],
                    'FIELD_NAME' => $arFields['FIELD_NAME'],
                )
            );
            if (!$res->Fetch()) {
                $ob = new CUserTypeEntity;
                $res = $ob->Add($arFields);
                if (empty($res)) {
                    preExit($arFields, $ob->LAST_ERROR);
                    throw new Exception($ob->LAST_ERROR);
                }
            }
        }
    }

}
