<?php

namespace Project\Tools\Iblock\V2\Update;

use Project\Tools\Iblock\V2;
use Exception;

class Highload
{

    static public function add($hlName, $name = 'UF_NAME', $arData)
    {
        $table = V2\Highload::table($hlName);
        $arFilter = array();
        foreach ($arData as $value) {
            $arFilter[] = $value[$name];
        }
        $rsData = $table::getList(array(
            "filter" => array('=' . $name => $arFilter),
            "select" => array($name)
        ));
        $arFilter = array();
        while ($arItem = $rsData->Fetch()) {
            $arFilter[$arItem[$name]] = true;
        }
        foreach ($arData as $value) {
            if (empty($arFilter[$value[$name]])) {
                $result = $table::add($value);
                if (!$result->isSuccess()) {
                    throw new Exception(implode('<br>', $result->getErrorMessages()));
                }
            }
        }
    }

}
