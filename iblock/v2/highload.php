<?php

namespace Project\Tools\Iblock\V2;

use Bitrix\Highloadblock\HighloadBlockLangTable;
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Loader;
use Exception;

class Highload
{

    static public function table($name) {
        static $arClass = array();
        if (empty($arClass[$name]) and Loader::includeModule('highloadblock')) {
            $arData = HighloadBlockTable::getList(array('filter' => array('NAME' => $name)))->fetch();
            $arClass[$name] = HighloadBlockTable::compileEntity($arData)->getDataClass();
        }
        return $arClass[$name];
    }

}
