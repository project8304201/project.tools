<?php

namespace Project\Tools\Iblock;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use CIBlock;
use CIBlockType;
use Project\Tools\Utility\Cache;

class Type
{


    /**
     * Проверяет наличие типа ИБ по коду
     *
     * @param string $type  Тип ИБ
     * @param bool   $cache Использовать или нет кэширование
     *
     * @return integer
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function checkIBlockTypeByCode($type, $cache = true)
    {
        $types = self::getIBlocksTypeList($cache);

        if (!isset($types[$type])) {
            throw new ArgumentException(sprintf('IblockType with "%s" code  - not found', $type));
        }

        return $type;
    }

    /**
     * Получает список всех типов ИБ сайта
     *
     * @param bool $useCache Использовать или нет кэширование
     *
     * @return array
     * @throws \Exception
     */
    public static function getIBlocksTypeList($useCache = true)
    {
        return Cache::getCached(function () {
            $result = [];

            if (!Loader::includeModule('iblock')) {
                throw new LoaderException('Module iblock not installed');
            }

            $db = CIBlockType::GetList(
                ['ID' => 'ASC']
            );
            while ($item = $db->Fetch()) {
                if (!strlen($item['ID'])) {
                    continue;
                }
                $result[$item['ID']] = $item['ID'];
            }

            if ($result) {
                $db = CIBlock::GetList(
                    [],
                    [
                        'CHECK_PERMISSIONS' => 'N',
                        '=TYPE'              => $result,
                    ]
                );
                while ($item = $db->Fetch()) {
                    Tag::register(Tag::iblock($item['ID']));
                }
            }

            return $result;

        }, [], !$useCache);
    }

}
