<?php

namespace Project\Tools\Iblock;

use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Loader;
use CFile;
use CIBlockElement;
use Project\Tools\Utility\Cache;
use Project\Tools\Utility\Image;

class Element
{

    /**
     * @param $ID
     *
     * @return null
     * @throws \Exception
     */
    static function getByID($ID, $useCache = true)
    {
        return Cache::getCached(function () use ($ID) {
            Loader::includeModule('iblock');
            $arItem = CIBlockElement::GetByID($ID)->GetNext();
            if ($arItem) {
                return $arItem;
            }
            return '';
        }, [
            'PARAM' => $ID,
            'TAGS'  => Tag::element($ID),
        ], !$useCache);
    }

    /**
     * @param $ID
     *
     * @return string
     * @throws \Exception
     */
    static function getCodeByID($ID, $useCache = true)
    {
        return Cache::getCached(function () use ($ID) {
            Loader::includeModule('iblock');
            $arItem = ElementTable::getList([
                'select' => ['CODE'],
                'filter' => [
                    'ID' => $ID,
                    'SHOW_HISTORY' => 'Y'
                ],
            ])->Fetch();
            if ($arItem) {
                return $arItem['CODE'];
            }
            return 0;
        }, [
            'PARAM' => $ID,
            'TAGS'  => Tag::element($ID),
        ], !$useCache);
    }

    /**
     * @param $ID
     * @param $width
     * @param $height
     *
     * @return null
     * @throws \Exception
     */
    static function getPreviewPictureByID($ID, $width, $height, $useCache = true)
    {
        return Cache::getCached(function () use ($ID, $width, $height) {
            Loader::includeModule('iblock');
            $arItem = ElementTable::getList([
                'select' => ['PREVIEW_PICTURE', 'DETAIL_PICTURE'],
                'filter' => [
                    'ID' => $ID,
                    'SHOW_HISTORY' => 'Y'
                ],
            ])->Fetch();
            if ($arItem && ($arItem['PREVIEW_PICTURE'] || $arItem['DETAIL_PICTURE'])) {
                $arImg = CFile::GetFileArray($arItem["PREVIEW_PICTURE"] ?: $arItem['DETAIL_PICTURE']);
                $arImg['SRC'] = Image::resize($arImg['ID'], $width, $height);
                return $arImg;
            }
            return false;
        }, [
            'PARAM' => [$ID, $width, $height],
            'TAGS'  => Tag::element($ID),
        ], !$useCache);
    }

    /**
     * @param      $arFilter
     * @param bool $useCache
     *
     * @return null
     * @throws \Exception
     */
    public static function selectedRowsCount($arFilter, $useCache = true)
    {
        return Cache::getCached(function () use ($arFilter) {
            Loader::includeModule('iblock');
            $resCnt = CIBlockElement::GetList([], $arFilter, false, false,
                ["ID"]);
            return $resCnt->SelectedRowsCount();
        }, [
            'CACHE_TIME' => Cache::TIME_HOUR,
        ], !$useCache);
    }

    /**
     * @param      $arFilter
     * @param bool $useCache
     *
     * @return null
     * @throws \Exception
     */
    public static function getCount($arFilter, $useCache = true)
    {
        return Cache::getCached(function () use ($arFilter) {
            Loader::includeModule('iblock');
            $resMetaCount = CIBlockElement::GetList([], $arFilter, ['IBLOCK_ID']);
            return $resMetaCount->Fetch();
        }, [
            'CACHE_TIME' => Cache::TIME_HOUR,
        ], !$useCache);
    }

    /**
     * @param array $arOrder
     * @param array $arFilter
     * @param bool  $arGroupBy
     * @param bool  $arNavStartParams
     * @param array $arSelectFields
     *
     * @return null
     * @throws \Exception
     */
    public static function getList(
        $arOrder = ["SORT" => "ASC"],
        $arFilter = [],
        $arGroupBy = false,
        $arNavStartParams = false,
        $arSelectFields = [],
        $useCache = true
    ) {
        return Cache::getCached(function () use ($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields) {
            Loader::includeModule('iblock');
            $arSelectFields[] = 'ID';
            $res = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
            $arResult = [];
            while ($arItem = $res->GetNext()) {
                Tag::register(Tag::element($arItem['ID']));
                $arResult[] = $arItem;
            }
            return $arResult;
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => [$arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields],
        ], !$useCache);
    }

    /**
     * @param array $arOrder
     * @param array $arFilter
     * @param bool  $arGroupBy
     * @param bool  $arNavStartParams
     * @param array $arSelectFields
     *
     * @return null
     * @throws \Exception
     */
    public static function getListFetch(
        $arOrder = ["SORT" => "ASC"],
        $arFilter = [],
        $arGroupBy = false,
        $arNavStartParams = false,
        $arSelectFields = [],
        $useCache = true
    ) {
        return Cache::getCached(function () use ($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields) {
            Loader::includeModule('iblock');
            $arSelectFields[] = 'ID';
            $arFilter['SHOW_HISTORY'] = 'Y';
            $res = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
            $arResult = [];
            while ($arItem = $res->Fetch()) {
                Tag::register(Tag::element($arItem['ID']));
                $arResult[] = $arItem;
            }
            return $arResult;
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => [$arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields],
        ], !$useCache);
    }


    /**
     * @param      $arSort
     * @param      $arFilter
     * @param      $arSelect
     * @param bool $useCache
     *
     * @return null
     * @throws \Exception
     */
    public static function getListPropertis($arSort, $arFilter, $arSelect, $useCache = true)
    {
        return Cache::getCached(function () use ($arSort, $arFilter, $arSelect) {
            Loader::includeModule('iblock');
            $arSelectFields[] = 'ID';
            $arFilter['SHOW_HISTORY'] = 'Y';
            $rsItems = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
            $arResult = [];
            while ($obItem = $rsItems->GetNextElement()) {
                $arItem = $obItem->GetFields();
                Tag::register(Tag::element($arItem['ID']));
                $arItem["PROPERTIES"] = $obItem->GetProperties();
                $arResult[] = $arItem;
            }
            return $arResult;
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => [$arSort, $arFilter, $arSelect],
        ], !$useCache);
    }

    /**
     * @param       $arFilter
     * @param       $arSelect
     * @param array $arSort
     *
     * @return string
     * @throws \Exception
     */
    static function getListByFilter($arFilter, $arSelect, $arSort = [], $useCache = true)
    {
        pre($arFilter, $arSelect);
        return self::getList($arSort, $arFilter, false, false, $arSelect, $useCache);
    }

    /**
     * @param       $arFilter
     * @param       $arSelect
     * @param array $arSort
     *
     * @return null
     * @throws \Exception
     */
    static function getByFilter($arFilter, $arSelect, $arSort = [], $useCache = true)
    {
        return Cache::getCached(function () use ($arFilter, $arSelect, $arSort) {
            Loader::includeModule('iblock');
            $arSelect[] = 'ID';
            $arFilter['SHOW_HISTORY'] = 'Y';
            $resMeta = CIBlockElement::GetList($arSort, $arFilter, false, ['nTopCount' => 1], $arSelect);
            if ($arItem = $resMeta->GetNext()) {
                Tag::register(Tag::element($arItem['ID']));
                return $arItem;
            } else {
                return 0;
            }
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => [$arFilter, $arSelect, $arSort],
        ], !$useCache);
    }

    /**
     * @param       $arFilter
     * @param       $arSelect
     * @param array $arSort
     *
     * @return null
     * @throws \Exception
     */
    static function getFetchByFilter($arFilter, $arSelect, $arSort = [], $useCache = true)
    {
        return Cache::getCached(function () use ($arFilter, $arSelect, $arSort) {
            Loader::includeModule('iblock');
            $arSelect[] = 'ID';
            $arFilter['SHOW_HISTORY'] = 'Y';
            $resMeta = CIBlockElement::GetList($arSort, $arFilter, false, ['nTopCount' => 1], $arSelect);
            if ($arItem = $resMeta->GetNext()) {
                Tag::register(Tag::element($arItem['ID']));
                return $arItem;
            } else {
                return 0;
            }
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => [$arFilter, $arSelect, $arSort],
        ], !$useCache);
    }

    /**
     * @param $IBLOCK_ID
     * @param $ELEMENT_ID
     * @param $arFilter
     * @param bool $useCache
     * @return null
     * @throws \Exception
     */
    static function getProperty($IBLOCK_ID, $ELEMENT_ID, $arFilter, $useCache = true)
    {
        return Cache::getCached(function () use ($IBLOCK_ID, $ELEMENT_ID, $arFilter) {
            Loader::includeModule('iblock');
            $arSelect[] = 'ID';
            $arFilter['SHOW_HISTORY'] = 'Y';
            $resMeta = CIBlockElement::GetProperty($IBLOCK_ID, $ELEMENT_ID, $by="sort", $order="asc", $arFilter);
            if ($arItem = $resMeta->Fetch()) {
                return $arItem;
            } else {
                return 0;
            }
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => [$IBLOCK_ID, $ELEMENT_ID, $arFilter],
        ], !$useCache);
    }

    static function getPropertyValue($IBLOCK_ID, $ELEMENT_ID, $arFilter, $useCache = true)
    {
        return Cache::getCached(function () use ($IBLOCK_ID, $ELEMENT_ID, $arFilter) {
            Loader::includeModule('iblock');
            $arSelect[] = 'ID';
            $resMeta = CIBlockElement::GetProperty($IBLOCK_ID, $ELEMENT_ID, $by="sort", $order="asc", $arFilter);
            if ($arItem = $resMeta->Fetch()) {
                return $arItem['VALUE'];
            } else {
                return 0;
            }
        }, [
            'CACHE_TIME' => Cache::TIME_DAY,
            'PARAM'      => [$IBLOCK_ID, $ELEMENT_ID, $arFilter],
        ], !$useCache);
    }
}