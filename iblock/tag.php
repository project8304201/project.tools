<?

namespace Project\Tools\Iblock;

use Project\Tools\Utility;

class Tag
{

    /**
     * @param $tags
     */
    static public function register(...$tags)
    {
        foreach ($tags as $tag) {
            Utility\Tag::registerTag($tag);
        }
    }

    /**
     * @param $tag
     *
     * @return string
     */
    static public function iblock($tag)
    {
        return 'iblock_id_' . $tag;
    }

    /**
     * @param $tag
     *
     * @return string
     */
    static public function section($tag)
    {
        return 'iblock_section_' . $tag;
    }

    /**
     * @param $tag
     *
     * @return string
     */
    static public function sectionList()
    {
        return 'iblock_section_list';
    }

    /**
     * @param $tag
     *
     * @return string
     */
    static public function element($tag)
    {
        return 'iblock_element_' . $tag;
    }

}
