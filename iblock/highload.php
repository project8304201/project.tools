<?php

namespace Project\Tools\Iblock;

use Bitrix\Main\Loader,
    Bitrix\Highloadblock\HighloadBlockTable;

class Highload
{

    static private $arClass = array();

    static public function table($name)
    {
        if (empty(self::$arClass[$name]) and Loader::includeModule('highloadblock')) {
            $arData = HighloadBlockTable::getList(array('filter' => array('NAME' => $name)))->fetch();
            self::$arClass[$name] = HighloadBlockTable::compileEntity($arData)->getDataClass();
        }
        return self::$arClass[$name];
    }

    static public function getNameByKey($hlName, $key)
    {
        static $arData = array();
        if (!isset($arData[$hlName])) {
            $arData[$hlName] = array();
            $rsData = self::table($hlName)::getList(array(
                "select" => array('UF_XML_ID', 'UF_NAME')
            ));
            while ($arItem = $rsData->Fetch()) {
                $arData[$hlName][$arItem['UF_XML_ID']] = $arItem['UF_NAME'];
            }
        }
        return $arData[$hlName][$key] ?? '';
    }

}
