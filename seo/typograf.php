<?

namespace Project\Tools\Seo;

class Typograf
{

    protected static function replaceQuote(&$content)
    {
        $pattern = defined('BX_UTF') ? 'u' : '';
        $content = preg_replace('~(&quot;)(.*)(&quot;)~imsU' . $pattern, '«$2»', $content);
        $content = preg_replace('~(&amp;quot;)(.*)(&amp;quot;)~imsU' . $pattern, '«$2»', $content);
        $content = preg_replace('~(")([^"]+)(")~imsU' . $pattern, '«$2»', $content);
    }

    public static function replace(&$content)
    {
        $pattern = defined('BX_UTF') ? 'u' : '';
        preg_match('~(<h1[^>]*>)(.*)(</h1>)~imsU' . $pattern, $content, $tmp);
        if (!empty($tmp[2])) {
            $search = $replace = $tmp[2];
            preg_match_all('~(<([^>]+)>)~imsU' . $pattern, $search, $tmp2);
            if (!empty($tmp2[2])) {
                $tmp2 = array_combine($tmp2[2], $tmp2[2]);
            }
            $string = preg_split('~[<>]~imsU' . $pattern, $search);
            foreach ($string as $item) {
                if (!empty($item) && !isset($tmp2[$item])) {
                    $replace2 = $item;
                    self::replaceQuote($replace2);
                    $replace = str_replace($item, $replace2, $replace);
//                    preExit($replace2, $item);
                }
            }
//            pre($search, $replace);
            if ($search !== $replace) {
                $content = str_replace($search, $replace, $content);
            }
        }

        preg_match('~(<title[^>]*>)(.*)(</title>)~imsU' . $pattern, $content, $tmp);
        if (!empty($tmp[2])) {
            $search = $replace = $tmp[2];
            self::replaceQuote($replace);
//            preExit($search, $replace);
            if ($search !== $replace) {
                $content = str_replace($search, $replace, $content);
            }
        }

        preg_match('~(<meta[^>]*name="description"[^>]*content=")([^"]*)("[^>]*>)~imsU' . $pattern, $content, $tmp);
        if (!empty($tmp[2])) {
            $search = $replace = $tmp[2];
            self::replaceQuote($replace);
//            preExit($search, $replace);
            if ($search !== $replace) {
                $content = str_replace($search, $replace, $content);
            }
        }
    }

}
