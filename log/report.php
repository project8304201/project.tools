<?php

namespace Project\Tools\Log;

use Bitrix\Main\Application;

class Report {

    static private $arPath = array();

    public static function setPath($type, $path) {
        self::$arPath[$type] = Application::getDocumentRoot() . $path;
    }

    private static function getPath($type) {
        return self::$arPath[$type] ?: Application::getDocumentRoot() . '/upload/tmp/project.tools/log/' . $type . '.txt';
    }

    public static function clear($type) {
        if (file_exists(self::getPath($type))) {
            unlink(self::getPath($type));
        }
    }

    public static function add($type, ...$result) {
        CheckDirPath(self::getPath($type));
        ob_start();
        foreach ($result as $value) {
            if (is_array($value) or is_object($value)) {
                print_r($value);
            } else {
                var_dump($value);
            }
        }
        file_put_contents(self::getPath($type), ob_get_clean() . PHP_EOL . file_get_contents(self::getPath($type)));
    }

}
