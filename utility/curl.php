<?php

namespace Project\Tools\Utility;

class Curl
{

    /**
     * @param string $url
     * @param array  $get
     * @param array  $header
     *
     * @return bool|string
     */
    static public function get(string $url, array $get, array $header = [])
    {
        return self::request($url . (strpos($url, '?') ? '&' : '?') . http_build_query($get), [], false, $header);
    }

    /**
     * @param string $url
     * @param array  $post
     * @param array  $header
     *
     * @return bool|string
     */
    static public function post(string $url, array $post = [], array $header = [])
    {
        return self::request($url, $post, $header);
    }

    /**
     * @param string $url
     * @param array  $post
     * @param array  $header
     *
     * @return mixed
     */
    static public function json(string $url, array $post = [], array $header = [])
    {
        $header[] = 'Content-Type:application/json';
        return json_encode(self::request($url, json_encode($post), $header));
    }

    /**
     * @param string $url
     * @param array  $post
     * @param array  $header
     *
     * @return mixed
     */
    static protected function request(string $url, array $post = [], array $header = [])
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, '');
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_HEADER, false);
        if ($post) {
//                pre($url, $post, $header);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        }
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
//            pre($code, htmlspecialchars($out), json_decode($out));
        curl_close($curl);
        return $out;
    }

}