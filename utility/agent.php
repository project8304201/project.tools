<?php

namespace Project\Tools\Utility;

use CAgent;

class Agent {

    static public function add(
    $name, // PHP function name
            $module = "", // module
            $period = "N", // check for agent execution count in period of time
            $interval = 86400, // time interval between execution
            $datecheck = "", // first check for execution time
            $active = "Y", // is the agent active or not
            $next_exec = "", // first execution time
            $sort = 100, // order
            $user_id = false, // user
            $existError = true // return error, if agent already exist
    ) {
        $res = CAgent::GetList(Array(), Array("NAME" => $name));
        if (!$row = $res->Fetch()) {
            CAgent::AddAgent(
                    $name, $module, $period, $interval, $datecheck, $active, $next_exec, $sort, $user_id, $existError
            );
//            pre(
//                    $name, $module, $period, $interval, $datecheck, $active, $next_exec, $sort, $user_id, $existError
//            );
        }
    }

}
