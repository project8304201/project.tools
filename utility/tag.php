<?php

namespace Project\Tools\Utility;

use Bitrix\Main\Application;

class Tag
{
    static private $path = false;
    static private $arTag = [];
    static private $isStart = false;

    /**
     * @param $path
     */
    static public function startTagCache($path)
    {
        self::$path = $path;
    }

    /**
     * @param $tag
     */
    static public function registerTag($tag)
    {
        self::$isStart = true;
        self::$arTag[$tag] = $tag;
    }

    /**
     * @throws \Bitrix\Main\SystemException
     */
    static public function endTagCache()
    {
        if (self::$isStart) {
            Application::getInstance()->getTaggedCache()->endTagCache();
            self::$isStart = false;
            self::$arTag = [];
        }
    }
}