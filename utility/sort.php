<?php

namespace Project\Tools\Utility;

use Bitrix\Main\Application;

class Sort {

    static public function init($name, $map, $default = '') {
        global $APPLICATION;
        $request = Application::getInstance()->getContext()->getRequest();
        $item = $request->get($name);
        if(empty($default)) {
            $default = key($map);
        }
        if (!isset($map[$item])) {
            $item = $default;
        }
        $map[$item]['select'] = true;
        foreach ($map as $key => &$value) {
            $value['url'] = $APPLICATION->GetCurPageParam($default == $key ? '' : $name . '=' . $key, array($name, 'clear_cache', 'bitrix_include_areas'));
        }
        $map[$item]['key'] = strtoupper($item);
        return array($map[$item], $map);
    }

}
