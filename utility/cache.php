<?php

namespace Project\Tools\Utility;

use Bitrix\Main\Application;
use Exception;
use ReflectionFunction;

/**
 * Модель кэша
 */
class Cache
{

    const TIME_DEFAULT = 60 * 60 * 24 * 31;
    const TIME_DAY = 60 * 60 * 24;
    const TIME_HOUR = 60 * 60;
    const CACHE_PATH = '/project.tools/';

    /**
     * Экземпляр кэша
     *
     * @var \Bitrix\Main\Data\Cache
     */
    protected $core = null;

    /**
     * Идентификатор кэша
     *
     * @var string
     */
    protected $id = '';

    /**
     * Каталог кэша
     *
     * @var string
     */
    protected $dir = '';

    /**
     * Время жизни кэша
     *
     * @var integer
     */
    protected $time = 0;

    /**
     * Конструктор
     *
     * @param mixed $cacheId   Идентификатор кэша
     * @param mixed $cacheDir  Каталог кэша
     * @param mixed $cacheTime Время жизни кэша
     * @param mixed $cacheTag  Код тега для тегированного кэша
     *
     * @throws \Bitrix\Main\SystemException
     */
    public function __construct($cacheId, $cacheDir, $cacheTime = 3600, $cacheTag = [])
    {
        $this->id = serialize($cacheId);
        $this->dir = str_replace(['/', '\\'], \DIRECTORY_SEPARATOR, $cacheDir);
        if ($cacheTag) {
            Application::getInstance()->getTaggedCache()->startTagCache($this->dir);
            foreach ((array)$cacheTag as $tag) {
                Application::getInstance()->getTaggedCache()->registerTag($tag);
            }
            Application::getInstance()->getTaggedCache()->endTagCache();
        }
        $this->time = (int)$cacheTime;
        $this->core = Application::getInstance()->getCache();
    }

    /**
     * Запускает кэширование
     *
     * @return boolean
     */
    public function start()
    {
        if (isset($_REQUEST['clear_cache']) && $_REQUEST['clear_cache'] == 'Y' && User::IsAdmin()) {
            $this->core->clean($this->id, $this->dir);
        }
        return $this->core->startDataCache($this->time, $this->id, $this->dir);
    }

    /**
     * Сохраняет данные в кэш
     *
     * @param mixed $data Данные для кэширования
     *
     * @return void
     */
    public function end($data)
    {
        return $this->core->endDataCache($data);
    }

    /**
     * Возвращает ранее закэшированные данные
     *
     * @return mixed
     */
    public function getVars()
    {
        return $this->core->getVars();
    }

    /**
     * Удаляет ранее проинициализированный кэш
     *
     * @return void
     */
    public function abort()
    {
        return $this->core->abortDataCache();
    }

    /**
     * @param $params
     */
    private static function initParam(&$params)
    {
        static $idSite = false;
        if (empty($idSite)) {
            $idSite = SITE_ID . '/' . sha1(Application::getDocumentRoot()) . '/';
        }

        if (!isset($params['CACHE_TIME'])) {
            $params['CACHE_TIME'] = self::TIME_DAY;
        }

        if (is_array($params['CACHE_ID'])) {
            $params['CACHE_ID'] = implode('-', $params['CACHE_ID']);
        }
        $params['CACHE_ID'] .= (int)isDebug();

        if (isset($params['PARAM'])) {
            $params['CACHE_ID'] .= sha1(serialize($params['PARAM']));
        }

        if (isset($params['CACHE_PATH'])) {
            $params['CACHE_PATH'] = $idSite . $params['CACHE_PATH'];
        } else {
            $params['CACHE_PATH'] = self::CACHE_PATH . $idSite . $params['CACHE_ID'];
        }
    }


    /*
     * Кеширование в цикле

$arCacache = $currentItems = $arResult = [];
foreach ($list as $ID) {
    $cache = new Cache([__METHOD__, $ID], __CLASS__, 60 * 30);
    if ($cache->start()) {
        $currentItems[] = $ID;
        $arCacache[$ID] = $cache;
    } else {
        if ($arData = $cache->getVars()) {
            if($arData['COUNTER']) {
                $arResult[$ID] = $arData;
            }
        }
    }
}

if ($currentItems) {
    $arFilter = [
        "IBLOCK_ID"           => 54,
        "PROPERTY_ARTICLE_ID" => $currentItems,
        "ACTIVE"              => "Y",
    ];
    unset($currentItems);
    $res = CIBlockElement::GetList(["SORT" => "ASC", "PROPERTY_PRIORITY" => "ASC"], $arFilter,
        ["ID", "NAME", "IBLOCK_ID", "PROPERTY_RATING", "PROPERTY_ARTICLE_ID"]);
    while ($ar_fields = $res->Fetch()) {
        if (isset($arResult[$ar_fields["PROPERTY_ARTICLE_ID_VALUE"]])) {
            $recalculate = intval((($arResult[$ar_fields["PROPERTY_ARTICLE_ID_VALUE"]]['RATING'] * $arResult[$ar_fields["PROPERTY_ARTICLE_ID_VALUE"]]['COUNTER']) + $ar_fields["PROPERTY_RATING_VALUE"]) / ($arResult[$ar_fields["PROPERTY_ARTICLE_ID_VALUE"]]['COUNTER'] + 1));
            $arResult[$ar_fields["PROPERTY_ARTICLE_ID_VALUE"]] = [
                "RATING"  => $recalculate,
                "COUNTER" => $arResult[$ar_fields["PROPERTY_ARTICLE_ID_VALUE"]]['COUNTER'] + 1,
            ];
        } else {
            $arResult[$ar_fields["PROPERTY_ARTICLE_ID_VALUE"]] = [
                "RATING"  => intval($ar_fields["PROPERTY_RATING_VALUE"]),
                "COUNTER" => 1,
            ];
        }
    }
    foreach ($arCacache as $ID => $cache) {
        if (isset($arResult[$ID])) {
            $cache->end($arResult[$ID]);
        } else {
            $cache->end([
                "RATING"  => 0,
                "COUNTER" => 0,
            ]);
        }
        unset($arCacache[$ID]);
    }
}
     *
     * @param array $params
     * @param bool  $refreshCache
     *
     * @return Cache
     * @throws \Bitrix\Main\SystemException
     */
    public static function get(array $params = [], $refreshCache = false)
    {
        if (!isset($params['CACHE_ID'])) {
            $params['CACHE_ID'] = '';
        }
        self::initParam($params);
        $cache = new self($params['CACHE_ID'], $params['CACHE_PATH'], $params['CACHE_TIME'], $params['TAGS'] ?? []);
        if ($refreshCache || (isset($_REQUEST['clear_cache']) && $_REQUEST['clear_cache'] == 'Y' && User::IsAdmin())) {
            $cache->clean($params['CACHE_ID'], $params['CACHE_PATH']);
        }
        return $cache;
    }

    /**
     * Обертка над кэшированием, упрощает инициализацию кэша
     * <code>
     *      Доступные ключи массива $params
     *
     *      CACHE_TIME integer время хранения результата в кэше
     *      CACHE_ID string уникальный идентификатор кэша
     *      CACHE_PATH string путь для хранения кэша
     *      TAGS boolean флаг использования тегирования
     * </code>
     * $property = Cache::getCached(function () use ($iblockId, $propertyCode) {
     * return $property;
     * }, ['PARAM' => arary($iblockId, $propertyCode)], !$useCache);
     *
     * @param callable $callback     функция выполняемая в случае отсутсвия закэшированного значения
     * @param array    $params       Параметры кэширования
     * @param bool     $refreshCache флаг принудительной очистки кэша
     *
     * @return null
     * @throws Exception
     */
    public static function getCached($callback, array $params = [], $refreshCache = false)
    {
        if (!isset($params['CACHE_ID'])) {
            $ref = new ReflectionFunction($callback);
            $params['CACHE_ID'] = md5($ref->getFileName() . $ref->getStartLine() . $ref->getEndLine());
        }
        self::initParam($params, $refreshCache);

        static $arMemory = [];
        if (isset($params['MEMORY']) and isset($arMemory[$params['CACHE_ID']])) {
            return $arMemory[$params['CACHE_ID']];
        }

        $cache = Application::getInstance()->getCache();
        if ($refreshCache || (isset($_REQUEST['clear_cache']) && $_REQUEST['clear_cache'] == 'Y' && User::IsAdmin())) {
            $cache->clean($params['CACHE_ID'], $params['CACHE_PATH']);
        }

        $result = null;
        if ($cache->startDataCache($params['CACHE_TIME'], $params['CACHE_ID'],
                $params['CACHE_PATH']) || $refreshCache) {
            try {
                if (isset($params['TAGS'])) {
                    Tag::startTagCache($params['CACHE_PATH']);
                }
                $result = $callback();
                if ($result === false) {
                    $cache->abortDataCache();
                } else {
                    $cache->endDataCache($result);
                    if (isset($params['TAGS'])) {
                        foreach ((array)$params['TAGS'] as $tag) {
                            Tag::registerTag($tag);
                        }
                    }
                }
                Tag::endTagCache();
            } catch (Exception $e) {
                $cache->abortDataCache();
                throw $e;
            }

        } else {
            $result = $cache->getVars();
        }

        if (isset($params['MEMORY'])) {
            $arMemory[$params['CACHE_ID']] = $result;
        }

        return $result;
    }

    /**
     * @param $arCode
     * @param $arResult
     * @param $arCache
     */
    static public function replaceCache($arCode, &$arResult, $arCache)
    {
        foreach ($arCode as $code) {
            if (isset($arCache[$code])) {
                $arResult[$code] = $arCache[$code];
            }
        }
    }

}