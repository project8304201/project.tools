<?php

namespace Project\Tools\Utility;

class Content
{

    static public function translate($str)
    {
        static $t = [
            '/' => '-',
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'jo',
            'ж' => 'gh',
            'з' => 'z',
            'и' => 'i',
            'й' => 'j',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'х' => 'x',
            'ц' => 'c',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'th',
            'ъ' => '',
            'ь' => '',
            'ы' => 'y',
            'э' => 'eh',
            'ю' => 'ju',
            'я' => 'ja',
        ];

        $new = '';
        $str = mb_strtolower(trim($str), 'UTF-8');
        $str = preg_replace('~\s~S', '_', $str);
        for ($i = 0, $c = mb_strlen($str, 'UTF-8'); $i < $c; $i++) {
            $s = mb_substr($str, $i, 1, 'UTF-8');
            if (isset($t[$s])) {
                $new .= $t[$s];
            } else {
                if ((ord($s) > 126) or (ord($s) == 20)) {
                    $new .= '_';
                } else {
                    $new .= $s;
                }
            }
        }
        return $new;
    }

    /**
     * @param $str
     *
     * @return string
     */
    static public function translit($str)
    {
        return Cutil::translit($str, "ru", ["replace_space" => "-", "replace_other" => "-"]);
    }

    /**
     * @param $arItem
     *
     * @return array
     */
    static public function toWin1251($arItem)
    {
        return array_map('trim', array_map(function ($v) {
            return iconv('UTF-8', 'WINDOWS-1251', $v);
        }, $arItem));
    }

    /**
     * @param $arItem
     *
     * @return array
     */
    static public function toUtf8($arItem)
    {
        return array_map('trim', array_map(function ($v) {
            return iconv('WINDOWS-1251', 'UTF-8', $v);
        }, $arItem));
    }


    static public function isutf8($str)
    {
        pre(mb_detect_encoding($str, mb_list_encodings(), false ));
//        preExit();
        return mb_detect_encoding($str, mb_list_encodings(), true)==='UTF-8';
        $utf8= iconv('WINDOWS-1251', 'UTF-8', $str);
        return $str===$utf8;
    }

}
