<?php

namespace Project\Tools\Utility;

class Dir
{

    /**
     * @param $path
     *
     * @return array
     */
    static public function scandir($path)
    {
        $arResult = [];
        if (is_file($path)) {
            $arResult[] = $path;
        } elseif (is_dir($path)) {
            foreach (scandir($path) as $item) {
                if ($item !== '.' && $item !== '..') {
                    $arResult[] = $path . $item;
                }
            }
        }
        return $arResult;
    }

    /**
     * @param $source
     * @param $path
     */
    static public function copy($source, $path)
    {
        foreach (self::scandir($source) as $file) {
            copy($file, $path . basename($file));
        }
    }
}
