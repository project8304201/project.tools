<?

namespace Project\Tools\Utility;

use Exception;
use Project\Tools\Config;

class Cron extends Settings
{

    /**
     * @param $callback
     */
    static public function agent($callback)
    {
        if (self::isStart()) {
            try {
                $callback();
            } catch (Exception $e) {
            }
            self::stop();
        }
    }

    /**
     * @return bool
     */
    static public function isStart()
    {
        $time = self::get('cron', 0);
        if (empty($time) or ($time + Config::cronTimeOut()) < time()) {
            self::next();
            return true;
        }
        return false;
    }

    /**
     *
     */
    static public function next()
    {
        static $time = 0;
        if (empty($time) or ($time + 2) < time()) {
            self::set('cron', $time = time());
        }
    }

    /**
     *
     */
    static public function stop()
    {
        self::clear('cron');
    }

}
