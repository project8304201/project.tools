<?

namespace Project\Tools\Utility;


use Bitrix\Main\Application;

class Settings
{

    /**
     * @param string $path
     *
     * @return string
     */
    static protected function getPath($path = '') {
//        pre($path);
        $path = Application::getDocumentRoot() . '/upload/tmp/settings/' . sha1(__CLASS__) . ($path ? sha1($path) : '');
//        pre($path);
        CheckDirPath($path);
        return $path;
    }

    /**
     * @param        $path
     * @param string $default
     *
     * @return bool|false|mixed|string
     */
    static public function get($path, $default = '') {
        $path = self::getPath($path);
        return file_exists($path) ? file_get_contents($path) : $default;
    }

    /**
     * @param $path
     */
    static public function clear($path) {
        unlink(self::getPath($path));
    }

    /**
     * @param $path
     * @param $data
     */
    static public function set($path, $data) {
        file_put_contents(self::getPath($path), $data);
    }

}
