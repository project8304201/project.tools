<?php

namespace Project\Tools\Utility;

use CUser;

class User
{

    static private $userId = null;

    /**
     * @return bool|null
     */
    static public function isAdmin()
    {
        global $USER;
        if (empty($USER)) {
            $USER = new CUser();
        }
        return $USER->isAdmin();
    }

    /**
     * @param $userId
     */
    static public function authorize($userId)
    {
        global $USER;
        if (empty($USER) or !is_a($USER, 'cUser')) {
            $USER = new CUser;
        }
        if (CUser::GetID() == $userId) {
            return;
        }
        if (empty(self::$userId)) {
            self::$userId = (int)CUser::GetID();
        }
        if (self::$userId) {
            $USER->Logout();
        }
        $USER->Authorize($userId);
    }

    /**
     *
     */
    static public function logout()
    {
        global $USER;
        $USER->Logout();
        if (self::$userId) {
            $USER->Authorize(self::$userId);
        }
        self::$userId = null;
    }

}
