<?php

namespace Project\Tools\Parse;

use Bitrix\Main\Application;

class Content {

    static public function parseFloat($d) {
        return str_replace(',', '.', $d);
    }

    static public function getMeta($url) {
        $arResult = array();
        $path = Application::getDocumentRoot() . '/upload/tmp/project.tools/' . sha1($url);
        if (file_exists($path)) {
            $content = file_get_contents($path);
        } else {
            CheckDirPath($path);
            file_put_contents($path, $content = file_get_contents($url));
        }
        preg_match('~(<title>(.*)</title>)~ismuUS', $content, $tmp);
        if (!empty($tmp[2])) {
            $arResult['title'] = $tmp[2];
        }
        preg_match('~(<meta name="description" content="([^"]+)"\s*/>)~ismuUS', $content, $tmp);
        if (!empty($tmp[2])) {
            $arResult['description'] = $tmp[2];
        }
        preg_match('~(<meta name="keywords" content="([^"]+)"\s*/>)~ismuUS', $content, $tmp);
        if (!empty($tmp[2])) {
            $arResult['keywords'] = $tmp[2];
        }
        return $arResult;
    }

    static public function uploadSrcImage($content, $domen, $url = '') {
        $content = preg_replace_callback('~src="(' . $url . '([^"]*))"~isS', function($preg) use($domen) {
            if (count($preg) == 3) {
                $file = '/upload/project.tools/' . $preg[2];
                $path = Application::getDocumentRoot() . $file;
                if (!file_exists($path)) {
                    CheckDirPath($path);
                    file_put_contents($path, file_get_contents($domen . $preg[2]));
                    if (!file_exists($path)) {
                        return $preg[0];
                    }
                }
                return 'src="' . $file . '"';
            }
            return $preg[0];
        }, $content);
        $content = preg_replace_callback('~(url\([\'\"]?)' . $url . '([^"\)]*)([\'\"]?\))~isS', function($preg) use($domen) {
            if (count($preg) == 4) {
                $file = '/upload/project.tools/' . $preg[2];
                $path = Application::getDocumentRoot() . $file;
                if (!file_exists($path)) {
                    CheckDirPath($path);
                    file_put_contents($path, file_get_contents($domen . $preg[2]));
                    if (!file_exists($path)) {
                        return $preg[0];
                    }
                }
                return $preg[1] . $file . $preg[3];
            }
            return $preg[0];
        }, $content);
        return $content;
    }

}
