<?php

namespace Project\Tools\Parse;

use CFile,
    Bitrix\Main\Application;

class Image {

    static public function upload($file, $domen) {
        $path = Application::getDocumentRoot() . '/upload/tmp/project.tools/' . $file;
        if (strlen($path) or ! file_exists($path)) {
            CheckDirPath($path);
            $content = file_get_contents($domen . $file);
            if (empty($content)) {
                $file = implode('/', array_map('urlencode', explode('/', $file)));
                $content = file_get_contents($domen . $file);
            }
            file_put_contents($path, file_get_contents($domen . $file));
            if (!file_exists($path)) {
                return false;
            }
        }
        return CFile::MakeFileArray($path);
    }

}
