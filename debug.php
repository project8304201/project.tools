<?php

if (defined('Project\Debug\IS_START')) {
    return;
}

define('Project\Debug\IS_START', true);

define('IS_DEBUG_FILE', Bitrix\Main\Application::getDocumentRoot() . '/local/test/debug.php');
define('IS_DEBUG_FILE_ADMIN', Bitrix\Main\Application::getDocumentRoot() . '/local/test/debug.admin.txt');
define('IS_DEBUG_FILE_1С', Bitrix\Main\Application::getDocumentRoot() . '/local/test/debug.1c.txt');
define('IS_DEBUG_PAY_FILE', Bitrix\Main\Application::getDocumentRoot() . '/local/test/pay.txt');

//AddEventHandler("main", "OnBeforeLocalRedirect", function(&$url, $skip_security_check, $bExternal) {
//    preExit(func_get_args());
//});

/*
function set_cookie(name, value, exp_y, exp_m, exp_d, path, domain, secure) {
    var cookie_string = name + "=" + escape(value);
    if (exp_y) {
        var expires = new Date(exp_y, exp_m, exp_d);
        cookie_string += "; expires=" + expires.toGMTString();
    }
    if (path)
        cookie_string += "; path=" + escape(path);
    if (domain)
        cookie_string += "; domain=" + escape(domain);
    if (secure)
        cookie_string += "; secure";
    document.cookie = cookie_string;
}
set_cookie('is-debug-3985', '2352536', 2053, 01, 15, '/');
 */

function isDebug()
{
//    return false;
//    return true;
    static $is = null;
    if (is_null($is)) {
        if (0 and (isset($_COOKIE['is-debug-3985']) and $_COOKIE['is-debug-3985'] == '2352536')) {
            $e = new Exception();
            ?>
            <pre><?= $e->getTraceAsString() ?></pre><?
            exit;
        }
        global $USER;
        if (!$USER || !($USER instanceof CUser)) {
            $USER = new CUser();
        }
        if (php_sapi_name() === "cli"
            or defined('IS_DEBUG1')
            or ($USER && $USER instanceof CUser && in_array($USER->GetId(), Project\Tools\Config::adminDebug()))
            or (isset($_COOKIE['is-debug-3985']) and $_COOKIE['is-debug-3985'] == '2352536')
            or (isset($_GET['is-debug-3985']) and $_GET['is-debug-3985'] == '2352536')
        ) {
            $is = true;
        } else {
            $is = false;
        }
    }
    return $is;
}

function isStartCli() {
    return php_sapi_name() === "cli";
}

function pre()
{
    if (!isDebug()) {
        return;
    }
//    preTrace();
//    exit;
    echo '<pre>';
    foreach (func_get_args() as $value) {
        if (is_array($value) or is_object($value)) {
            print_r($value);
        } else {
            var_dump($value);
        }
    }
    echo '</pre>';
}

function pre2()
{
//    exit;
    echo '<pre>';
    foreach (func_get_args() as $value) {
        if (is_array($value) or is_object($value)) {
            print_r($value);
        } else {
            var_dump($value);
        }
    }
    echo '</pre>';
}

function preSqlTrace($isDebug=false)
{
    if (!isDebug()) {
        return;
    }
    static $start = true;
    static $tracker;
    if ($start) {
        if(empty($isDebug)) {
            preClear();
        }
        $tracker = Bitrix\Main\Application::getConnection()->startTracker(true);
        global $DB;
        $DB->ShowSqlStat = true;
    } else {
        Bitrix\Main\Application::getConnection()->stopTracker();
        $count = $time = 0;
        foreach ($tracker->getQueries() as $query) {
            $count++;
            $time += $query->getTime();
        }
//        pre($isDebug);
        if($isDebug) {
            preDebug($count, number_format($time, 7));
        }else {
            pre($count, number_format($time, 7));
        }
        foreach ($tracker->getQueries() as $query) {
            echo $query->getSql();
//            $trace = array_slice($query->getTrace(), 0, 1);
            if($isDebug) {
                preDebug(number_format($query->getTime(), 7));
                preExit(number_format($query->getTime(), 7));
            }else {
                pre(number_format($query->getTime(), 7));
            }
//            pre(number_format($query->getTime(), 7), $query->getTrace());
        }
        if(empty($isDebug)) {
            preExit();
        }
    }
    $start = !$start;
}

function preSqlTraceImport($max = 0, $isTrace = false)
{
    $max = 0.1;
//    $isTrace = true;
    if (!isDebug()) {
        return;
    }
    static $start = true;
    static $tracker;
    if ($start) {
        preClear();
        $tracker = Bitrix\Main\Application::getConnection()->startTracker(true);
        global $DB;
        $DB->ShowSqlStat = true;
    } else {
        Bitrix\Main\Application::getConnection()->stopTracker();
        $count = $time = 0;
        $arQuery = [];
        foreach ($tracker->getQueries() as $query) {
            $count++;
            $time += $query->getTime();
            $id = str_replace('  ', ' ', trim($query->getSql()));
            $id = substr($id, 0, 40);
            if (isset($arQuery[$id])) {
                $arQuery[$id]['time'] += $query->getTime();
                $arQuery[$id]['count']++;
            } else {
                $arQuery[$id] = [
                    'query' => $query->getSql(),
                    'trace' => $query->getTrace(),
                    'time'  => $query->getTime(),
                    'count' => 1,
                ];
            }
        }
        pre($count, number_format($time, 7), COption::GetOptionString("sale", "1C_INTERVAL", 30));
        foreach ($tracker->getQueries() as $query) {
            if ($max && $max > $query->getTime()) {
                continue;
            }
//            pre(trim($query->getSql()), number_format($query->getTime(), 7));
//            if ($isTrace) {
//                pre(trim($query->getTrace()));
//            }
        }
        foreach ($arQuery as $item) {
            if ($max && $max > $item['time']) {
                continue;
            }
            pre('time: ' . number_format($item['time'], 7), $item['count'] . ': ' . trim($item['query']));
            if ($isTrace) {
                pre($item['trace']);
            }
        }
        preExit(array_keys($arQuery));
    }
    $start = !$start;
}

function preTrace($isResurn = false)
{
    if (!isDebug()) {
        return;
    }
    $e = new Exception();
    if($isResurn) {
        return $e->getTraceAsString();
    } else {
        ?>
        <pre><?= $e->getTraceAsString() ?></pre><?
    }
}

function preClear()
{
    if (!isDebug()) {
        return;
    }
    $GLOBALS['APPLICATION']->RestartBuffer();
//    preTrace();
}

function preTime()
{
    if (!isDebug()) {
        return;
    }
    if (func_num_args()) {
        pre(func_get_args());
    }
    static $start = false;
    if (empty($start)) {
        $start = microtime(true);
    } else {
        $finish = microtime(true);
        $delta = $finish - $start;
        pre($delta . ' сек.');
    }
//    pre(date('d.m.Y H:i:s') . ' ' . (memory_get_usage(true) / 1024 / 2014));
}

function preError()
{
    if (!isDebug()) {
        return;
    }
//    preTrace();
//    exit;
    global $APPLICATION;
    if ($APPLICATION->GetException()) {
        preTrace();
        echo '<pre>';
        var_dump('GetException');
        pre(trim($APPLICATION->GetException()->GetString()));
        echo '</pre>';
        exit;
    }
}

function preCore()
{
    if (!defined('IS_DEBUG')) {
        return;
    }
//    preTrace();
//    exit;
    echo '<pre>';
    foreach (func_get_args() as $value) {
        if (is_array($value) or is_object($value)) {
            print_r($value);
        } else {
            var_dump($value);
        }
    }
    echo '</pre>';
}

function preDate($time = null, $isPre = true)
{
    if (!isDebug()) {
        return;
    }
//    preTrace();
//    exit;
    if ($isPre) {
        echo '<pre>';
    }
    echo PHP_EOL . date('d.m.Y H:i:s') . PHP_EOL;
    if ($isPre) {
        echo '</pre>';
    }
}

function preDate2($time = null, $isPre = true)
{
    if ($isPre) {
        echo '<pre>';
    }
    echo PHP_EOL . date('d.m.Y H:i:s') . PHP_EOL;
    if ($isPre) {
        echo '</pre>';
    }
}

function preHtml()
{
    if (!isDebug()) {
        return;
    }
    echo '<pre>';
    foreach (func_get_args() as $value) {
        $value = preg_replace('~(</[^>]+>)~', '$1' . PHP_EOL, $value);
        $value = str_replace('/><', '/>' . PHP_EOL . '<', $value);
        echo htmlspecialchars($value);
    }
    echo '</pre>';
}

function preDebugStart()
{
    file_put_contents(IS_DEBUG_FILE, PHP_EOL . date('d.m.Y H:i:s'));
//    file_put_contents(IS_DEBUG_FILE, PHP_EOL . date('d.m.Y H:i:s'), FILE_APPEND);
}

function preDebug()
{
//    if (!isDebug())
//        return;
    ob_start();
    $e = new Exception();
    /*?>
    <pre><?= $e->getTraceAsString() ?></pre>
    <pre><?= PHP_EOL . date('d.m.Y H:i:s') . PHP_EOL; ?></pre>
    <?*/
//    echo '<pre>';
    foreach (func_get_args() as $value) {
        if (is_array($value) or is_object($value)) {
            print_r($value);
        } else {
            var_dump($value);
        }
    }
//    echo '</pre>';
    if(!file_exists(IS_DEBUG_FILE)) {
        file_put_contents(IS_DEBUG_FILE, '<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");?><pre>');
    }
    file_put_contents(IS_DEBUG_FILE, ob_get_clean(), FILE_APPEND);
}

function preDebugAdmin()
{
    ob_start();_
    ?>
    <pre><?= PHP_EOL . date('d.m.Y H:i:s') . PHP_EOL; ?></pre>
    <?
    global $USER, $APPLICATION;
    var_dump($APPLICATION->GetCurPageParam('', array(), false,), $USER->GetId());
//    echo '<pre>';
    foreach (func_get_args() as $value) {
        if (is_array($value) or is_object($value)) {
            print_r($value);
        } else {
            var_dump($value);
        }
    }
    file_put_contents(IS_DEBUG_FILE_ADMIN, ob_get_clean() . PHP_EOL . file_get_contents(IS_DEBUG_FILE_ADMIN));
}

function preDebug1cStart()
{
    file_put_contents(IS_DEBUG_FILE_1С, PHP_EOL . date('d.m.Y H:i:s'));
}

function preDebug1c()
{
//    if (!isDebug())
//        return;
    ob_start();
    preTrace();
    preDate(false);
//    echo '<pre>';
    foreach (func_get_args() as $value) {
        if (is_array($value) or is_object($value)) {
            print_r($value);
        } else {
            var_dump($value);
        }
    }
//    echo '</pre>';
    if(!file_exists(IS_DEBUG_FILE)) {
        file_put_contents(IS_DEBUG_FILE, '<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");?><pre>');
    }
    file_put_contents(IS_DEBUG_FILE, ob_get_clean(), FILE_APPEND);
}

function payDebug()
{
    ob_start();
    echo '<pre>';
    foreach (func_get_args() as $value) {
        if (is_array($value) or is_object($value)) {
            print_r($value);
        } else {
            var_dump($value);
        }
    }
    echo '</pre>';
    file_put_contents(IS_DEBUG_PAY_FILE, ob_get_clean() . PHP_EOL . file_get_contents(IS_DEBUG_PAY_FILE));
}

function preExit()
{
    if (!isDebug()) {
        return;
    }
    preTrace();
    echo '<pre>';
    foreach (func_get_args() as $value) {
        if (is_array($value) or is_object($value)) {
            print_r($value);
        } else {
            var_dump($value);
        }
    }
    echo '</pre>';
    exit;
}

function preExport()
{
    if (!isDebug()) {
        return;
    }
    preTrace();
    echo '<pre>';
    echo var_export(func_get_args(), true);
    echo '</pre>';
    exit;
}

function preExitOne()
{
    if (!isDebug()) {
        return;
    }
    $GLOBALS['APPLICATION']->RestartBuffer();
    echo '<pre>';
    foreach (func_get_args() as $value) {
        if (is_array($value) or is_object($value)) {
            print_r($value);
        } else {
            var_dump($value);
        }
    }
    echo '</pre>';
    exit;
}

function preMemory()
{
    pre('memory: ' . round(memory_get_usage() / 1024 / 1024, 3));
}

