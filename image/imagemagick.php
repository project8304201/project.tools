<?php

namespace Project\Tools\Image;

use Bitrix\Main\Application;
use CFile;
use Project\Tools\Utility\Cache;

class Imagemagick
{

    static public function resize($ID, $width, $height)
    {
        return Cache::getCached(function () use ($ID, $width, $height) {
            if (is_numeric($ID)) {
                $file = CFile::GetFileArray($ID);
                $img = $file['SRC'];
            } else {
                $img = $ID;
            }
            $newFile = '/upload/resize_cache/' . $width . '-' . $height . $img;
            $path = Application::getDocumentRoot();
            if(!file_exists($path . $newFile)) {
                CheckDirPath($path . $newFile);
                exec("convert {$path}{$img} -resize {$width}x{$height} {$path}{$newFile}");
                pre("convert {$path}{$img} -resize {$width}x{$height} {$path}{$newFile}");
            }
            return $newFile;
        }, [
            'PARAM' => [$ID, $width, $height],
        ]);
    }

}
