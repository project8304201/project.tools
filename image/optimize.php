<?php

namespace Project\Tools\Image;

use Bitrix\Main\Config\Option;
use CFile,
    Bitrix\Main\Application,
    Project\Tools\Config;
use Project\Tools\Utility\Cache;

class Optimize
{

    const PNG_LIMIT = 500;

    const OPTIMIZE = 1;
    const PROCESS = 2;
    const OFF = 3;

    static public function dataPath(string $src, string $dataPath)
    {
        $data = sha1($src);
        return $dataPath . 'data/' . substr($data, 0, 3) . '/' . $data;
    }

    static public function optimizeBackup(string $src)
    {
        $backup = str_replace(
            array('/upload/', '/local/templates/'),
            array('/upload/backup/', '/local/templates/backup/'),
            $src);
//        preExit($backup);
        if (!file_exists($backup)) {
            CheckDirPath($backup);
            copy($src, $backup);
        }
        pre($backup);
        return $backup;
    }

    static public function file(string $src)
    {
        $backup = self::optimizeBackup($src);
        $quality = 85;
        $path = dirname($src);
        $command = "export PATH=\$PATH:$(pwd)/node_modules/.bin && npx @squoosh/cli --mozjpeg '{quality:{$quality}}' -d {$path} {$src}  2>&1";
//        preExit($command);
        exec($command, $res);
        pre($command, $res);
        if (filesize($src) > filesize($backup)) {
//            preExit(filesize($backup), filesize($src));
            file_put_contents($src, file_get_contents($backup));
        }
        return array_slice($res, -2);
    }

    static public function filePng(string $src)
    {
        self::optimizeBackup($src);
        $path = dirname($src);
        $command = "convert {$src} -background 'rgba(255,255,255,255)' -flatten {$src} 2>&1";
        exec($command, $res);
        //pre($command, $res);
        $command = "export PATH=\$PATH:$(pwd)/node_modules/.bin && npx @squoosh/cli --oxipng auto -d {$path} {$src}  2>&1";
        exec($command, $res);
        //pre($command, $res);
        return array_slice($res, -2);
    }

    static public function optimize(string $src, string $dataPath)
    {
        //pre(func_get_args());
        switch ($type = substr($src, strrpos($src, '.') + 1)) {
            case 'jpg':
            case 'jpeg':
            case 'JPG':
            case 'JPEG':
                $data = self::dataPath($src, $dataPath);
                if (!file_exists($data) && file_exists($src)) {
                    pre(__FUNCTION__, $src, file_exists($src), date('d.m.Y H:i:s', fileatime($src)));
//                    $quality = strpos($src, '/resize_cache/') ? 75 : 75;
//                    $quality = strpos($src, '/85_85_1/') ? 85 : 75;
//                    $path = dirname($src);
                    switch ($type = substr($src, strrpos($src, '.') + 1)) {
                        case 'jpg':
                        case 'jpeg':
                        case 'JPG':
                        case 'JPEG':
                            $res = self::file($src);
                            if ($type === 'JPG' || $type === 'JPEG') {
                                $file = substr($src, 0, $type === 'JPG' ? -3 : -3) . ($type === 'JPG' ? 'jpg' : 'jpeg');
                                unlink($src);
                                rename($file, $src);
                            }
                            break;

                        case 'png':
                        case 'PNG':
//                            $res = self::filePng($src);
//                            if ($type === 'PNG') {
//                                $file = substr($src, 0, -3) . 'png';
//                                unlink($src);
//                                rename($file, $src);
//                            }
//                            preExit($data, $res);
                            break;
                    }

                    CheckDirPath($data);
                    file_put_contents($data, '');
                    pre($data, $res);
                    $history = $dataPath . date('Y-m') . '/' . date('d') . '.txt';
                    CheckDirPath($history);
                    file_put_contents($history, PHP_EOL . str_replace(Application::getDocumentRoot(), '', print_r($res, true)), FILE_APPEND);
                    pre($history);
                    return self::PROCESS;
                }
                return self::OPTIMIZE;
                break;
        }
        return self::OFF;
    }

    static public function optimizePng(string $src, string $dataPath)
    {
        static $fileCount = 0;
        //pre(func_get_args());
        switch ($type = substr($src, strrpos($src, '.') + 1)) {
            case 'png':
            case 'PNG':
                $fileCount++;
//                return self::OPTIMIZE;
                $data = self::dataPath($src, $dataPath);
//                if(file_exists($data)) {
//                    unlink($data);
//                }
//                return;
                if (!file_exists($data) && file_exists($src)) {
//                    if ($src!=='/var/www/fastuser/data/www/m65-casual.ru/upload/iblock/02f/jsw0af7aprb5x9y3561uafyu5lsc3fsw.png') {
//                        continue;
//                    }

                    pre(__FUNCTION__, $src, file_exists($src), date('d.m.Y H:i:s', fileatime($src)));
//                    $quality = strpos($src, '/resize_cache/') ? 75 : 75;
//                    $quality = strpos($src, '/85_85_1/') ? 85 : 75;
//                    $path = dirname($src);
//                    $command = "export PATH=\$PATH:$(pwd)/node_modules/.bin && npx @squoosh/cli --mozjpeg '{quality:{$quality}}' -d {$path} {$src}  2>&1";
//                    exec($command, $res);
//                    $res = array_slice($res, -2);

                    $res = self::filePng($src);
                    if ($type === 'PNG') {
                        $file = substr($src, 0, -3) . 'png';
                        unlink($src);
                        rename($file, $src);
                    }
                    $res2 = self::file($src);
                    rename(
                        substr($src, 0, -3) . 'jpg',
                        $src . '.jpg'
                    );
                    $file = substr($src, strlen(Application::getDocumentRoot()));
                    file_put_contents(
                        Application::getDocumentRoot() . '/local/image/png/' . date('Y-m-d') . 'image' . ceil($fileCount / self::PNG_LIMIT) . '.html',
                        '<div style="float: left;"><img src="' . $file . '" width="150px" heigth="150px"><img src="' . $file . '.jpg" width="150px" heigth="150px"></div>',
                        FILE_APPEND
                    );
//                    pre(
//                        substr($src, 0, -3) . 'jpg',
//                        $src . '.jpg'
//                    );
                    pre($data, $res, $res2);

                    CheckDirPath($data);
                    file_put_contents($data, '');
                    //pre($data, $res);
                    return self::PROCESS;
                }
                return self::OPTIMIZE;
                break;
        }
        return self::OFF;
    }

}
