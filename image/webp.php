<?php

namespace Project\Tools\Image;

use Bitrix\Main\Application;
use CFile;
use Project\Tools\Utility\Cache;

class Webp
{

    static private $arSize = [400, 480, 768];

    static public function getData($ID, $width, $height, $arParam = [])
    {
        $arSize = self::$arSize;
        return Cache::getCached(function () use ($ID, $width, $height, $arParam, $arSize) {
            $arFile = [];
            $arFile['SRC'] = self::get(
                $ID,
                $width,
                $height,
                $arParam['type'] ?? BX_RESIZE_IMAGE_PROPORTIONAL,
                $arParam['quality'] ?? false
            );
            foreach ($arSize as $size) {
                if ($width > $size) {
                    $arFile[$size] = self::get(
                        $ID,
                        $size,
                        ceil($size * $height / $width),
                        $arParam['type'] ?? BX_RESIZE_IMAGE_PROPORTIONAL,
                        $arParam['quality'] ?? false
                    );
                }
            }
            return $arFile;
        }, [
            'PARAM' => [$ID, $width, $height, $arParam],
        ]);
    }

    static public function view($ID, $width, $height, $arParam = [])
    {
        self::viewData(self::getData($ID, $width, $height, $arParam), $arParam);
    }

    static public function viewData($arFile, $arParam)
    {
        ?>
        <picture <?= $arParam['param'] ?? '' ?>>
            <? foreach (self::$arSize as $size) {
                if ($arFile[$size]) { ?>
                    <source media="(max-width: <?= $size ?>>px)" type="image/webp"
                            srcset="<?= $arFile[$size]["WEBP"] ?>">
                    <source media="(max-width: <?= $size ?>px)" type="image/jpeg" srcset="<?= $arFile[$size]["JPG"] ?>">
                <? }
            } ?>
            <source type="image/webp" srcset="<?= $arFile['SRC']['WEBP'] ?>">
            <img data-src="<?= $arFile['SRC']['JPG'] ?>" alt="<?= $arParam['alt'] ?? false ?>"
                 title="<?= $arParam['title'] ?? false ?>">
        </picture>
        <?
    }


    static public function get(
        $ID,
        $width,
        $height,
        $type = BX_RESIZE_IMAGE_PROPORTIONAL,
        $quality = false,
        $useCache = true
    ) {
        if (empty($quality)) {
            $quality = Image::getQuality();
        }
        $func = function () use ($ID, $width, $height, $type, $quality) {
            if ($width && $height) {
                $img = Image::resize($ID, $width, $height, $type, $quality);
            } else {
                if (is_numeric($ID)) {
                    $file = CFile::GetFileArray($ID);
                    $typeImg = $file['CONTENT_TYPE'];
                    $img = $file['SRC'];
                } else {
                    $img = $ID;
                }
            }
            $webSrc = '/upload/resize_cache/custom' . $img . ($width ? '-' . $width : '') . ($height ? '-' . $height : '') . '-' . $quality . '.webp';

            if (!file_exists(Application::getDocumentRoot() . $webSrc)) {
                if (empty($typeImg)) {
                    $typeImg = mime_content_type(Application::getDocumentRoot() . $img);
                }
                if ($typeImg === 'image/png') {
                    $im = imagecreatefrompng(Application::getDocumentRoot() . $img);
                } elseif ($typeImg === 'image/jpeg') {
                    $im = imagecreatefromjpeg(Application::getDocumentRoot() . $img);
                } else {
                    return false;
                }
                CheckDirPath(Application::getDocumentRoot() . $webSrc);
                imagewebp($im, Application::getDocumentRoot() . $webSrc, $quality);
                imagedestroy($im);
            }
            return [
                'JPG'  => $img,
                'WEBP' => $webSrc,
            ];
        };
        if ($useCache) {
            return Cache::getCached($func, [
                'PARAM' => [$ID, $width, $height, $type, $quality],
            ]);
        } else {
            return $func;
        }

    }

    static public function resize($ID, $width, $height, $type = BX_RESIZE_IMAGE_PROPORTIONAL, $quality = false)
    {
        if (empty($quality)) {
            $quality = Image::getQuality();
        }
        return Cache::getCached(function () use ($ID, $width, $height, $type, $quality) {
            $img = Image::resize($ID, $width, $height, $type, $quality);
            $type = mime_content_type($img);
            preExit($type);
            if ($type === 'image/png') {
                self::$isPng = true;

                return true;
            } elseif ($type === 'image/jpeg') {
                self::$isPng = false;

                return true;
            }
        }, [
            'PARAM' => [$ID, $width, $height, $type, $quality],
        ]);

        CFile::GetFileArray($file);
        return CFile::ResizeImageGet($ID, ['width' => $width, 'height' => $height], $type, false)['src'];
    }

}
