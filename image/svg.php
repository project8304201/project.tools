<?php

namespace Project\Tools\Image;

use Bitrix\Main\Application;
use Project\Tools\Utility\Cache;
use SimpleXMLElement;

class Svg
{

    /**
     * @param $file
     * @param $height
     *
     * @return float|int
     * @throws \Exception
     */
    static public function getSizeHeight($file, $height)
    {
        $arFile = self::getSize($file);
        if ($arFile['width'] && $arFile['height']) {
            return $arFile['width'] / $arFile['height'] * $height;
        } else {
            return 0;
        }
    }

    /**
     * @param $file
     * @param $width
     * @return float|int
     * @throws \Exception
     */
    static public function getSizeWidth($file, $width)
    {
        preClear();
        $arFile = self::getSize($file);
        preExit($arFile);
        if ($arFile['width'] && $arFile['height']) {
            return $arFile['height'] / $arFile['width'] * (int)$width;
        } else {
            return 0;
        }
    }

    /**
     * @param $file
     *
     * @return null
     * @throws \Exception
     */
    static public function getSize($file)
    {
        return Cache::getCached(function () use ($file) {
            try {
                $svg = new SimpleXMLElement(file_get_contents(Application::getDocumentRoot() . $file));
            } catch (Exception $e) {
                return false;
            }
            return [
                'width'  => (int)$svg['width'],
                'height' => (int)$svg['height'],
            ];
        }, [
            'PARAM' => $file,
        ]);
    }

}
