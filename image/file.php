<?php

namespace Project\Tools\Image;

use Bitrix\Main\Application;
use CFile;
use Project\Tools\Utility\Cache;
use Stalker\Main\Config;

class File
{

    /**
     * @param $ID
     *
     * @return null
     * @throws \Exception
     */
    static public function getPath($ID)
    {
        return Cache::getCached(function () use ($ID) {
            return CFile::GetPath($ID);
        }, [
            'PARAM' => $ID,
        ]);
    }

    static public function localUrl($url) {
        $file = '/upload/tmp/'. Config::MODULE_ID .'/local/'. sha1($url) .'.' .substr(strrchr($url, '.'), 1);
        CheckDirPath(Application::getDocumentRoot() . $file);
        file_put_contents(Application::getDocumentRoot() . $file, file_get_contents($url));
        return $file;
    }

}
