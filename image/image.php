<?php

namespace Project\Tools\Image;

use Bitrix\Main\Config\Option;
use CFile,
    Bitrix\Main\Application,
    Project\Tools\Config;
use Project\Tools\Utility\Cache;

class Image
{

    /**
     * @return bool|string
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    static public function getQuality()
    {
        static $defaultQuality = false;
        if (empty($defaultQuality)) {
            $defaultQuality = Option::get('main', 'image_resize_quality', 95);
        }
        return $defaultQuality;
    }

    /**
     * @param $ID
     *
     * @return mixed
     */
    static private function isWatermark($ID)
    {
        return Config::isWarermark();
    }

    /**
     * @param      $ID
     * @param      $width
     * @param      $height
     * @param bool $watermark
     * @param bool $quality
     *
     * @return mixed|string
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    static public function watermark2($ID, $width, $height, $watermark = false, $quality = false)
    {
        if (self::isWatermark($ID)) {
            if (empty($watermark)) {
                $watermark = Config::warermarkPath();
            }
            if (empty($quality)) {
                $quality = self::getQuality();
            }
            $path = cFile::GetPath($ID);
            $newPath = Application::getDocumentRoot() . ($img = str_replace('/upload/',
                    '/upload/resize_cache/warermark/' . sha1($width . $height . $watermark . $quality) . '/', $path));
            CFile::ResizeImageFile(
                Application::getDocumentRoot() . $path, $newPath, ['width' => $width, 'height' => $height],
                BX_RESIZE_IMAGE_PROPORTIONAL, [
                "name"        => "watermark",
                "position"    => "center",
                "size"        => "resize",
                'coefficient' => Config::warermarkСoefficient(),
                "file"        => Application::getDocumentRoot() . $watermark,
            ], $quality
            );
            return $img;
        } else {
            return self::resize($ID, $width, $height);
        }
    }

    /**
     * @param      $ID
     * @param      $width
     * @param      $height
     * @param int  $type
     * @param bool $quality
     *
     * @return null
     * @throws \Exception
     */
    static public function resizeCache($ID, $width, $height, $type = BX_RESIZE_IMAGE_PROPORTIONAL, $quality = false, $refreshCache=false)
    {
        return Cache::getCached(function () use ($ID, $width, $height, $type, $quality) {
            return Image::resize($ID, $width, $height, $type, $quality);
        }, [
            'PARAM' => [$ID, $width, $height, $type, $quality],
        ], $refreshCache);
    }

    /**
     * @param      $ID
     * @param      $width
     * @param      $height
     * @param int  $type
     * @param bool $quality
     *
     * @return null
     * @throws \Exception
     */
    static public function resizeCacheSize($ID, $width, $height, $type = BX_RESIZE_IMAGE_PROPORTIONAL, $quality = false)
    {
        return Cache::getCached(function () use ($ID, $width, $height, $type, $quality) {
            return CFile::ResizeImageGet($ID, ['width' => $width, 'height' => $height], $type, true);
        }, [
            'PARAM' => [$ID, $width, $height, $type, $quality],
        ]);
    }

    /**
     * @param      $ID
     * @param      $width
     * @param      $height
     * @param int  $type
     * @param bool $quality
     *
     * @return string
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    static public function resize($ID, $width, $height, $type = BX_RESIZE_IMAGE_PROPORTIONAL, $quality = false)
    {
        if (empty($quality)) {
            $quality = self::getQuality();
        }
        if (is_numeric($ID)) {
            return CFile::ResizeImageGet($ID, ['width' => $width, 'height' => $height], $type, false, false, false,
                $quality)['src'];
        } else {
            $file = '/upload/resize_cache/custom/' . ($width ? '-' . $width : '') . ($height ? '-' . $height : '') . '-' . $quality . $ID;
            $newPath = Application::getDocumentRoot() . $file;
            CFile::ResizeImageFile(
                Application::getDocumentRoot() . $ID, $newPath, ['width' => $width, 'height' => $height],
                $type, [], $quality
            );
//            self::optimize($newPath);
            return $file;
        }
    }

    /**
     * @param $src
     *
     * @return null
     * @throws \Exception
     */
    static public function getImagesize($src)
    {
        return Cache::getCached(function () use ($src) {
            $arFile = getimagesize(Application::getDocumentRoot() . $src);
            $arFile['src'] = $src;
            return $arFile;
        }, [
            'PARAM' => $src
        ]);
    }

    /**
     * @param $src
     */
//    static public function optimize($file)
//    {
//        switch (substr($file, strrpos($file, '.') + 1)) {
//            case 'jpg':
//            case 'jpeg':
//                exec("/usr/bin/jpegoptim --strip-all -t -pm95 $file 2>&1", $res);
//                break;
//
//            case 'png':
////                    exec("/usr/bin/optipng -o3 $file 2>&1", $res);
//                break;
//
//            case 'gif':
////                    exec("/usr/bin/gifsicle -O2 $file -o $file 2>&1", $res);
//                break;
//
//            default:
//                break;
//        }
//    }


}
