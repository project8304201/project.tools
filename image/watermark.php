<?php

namespace Project\Tools\Image;

class Watermark {

    const PATH = '/upload/watermark/';
    const LOGO = 'stamp.png';
    const SIZE = 60;
    const NOTICE = 'bania-msk.ru';
    const ID = 641;

    static public function is($ID) {
        return ($ID < 1596 and $ID >= 642) or ( $ID <= 1632 and $ID > 1596) or $ID >= 2172;
    }

    static public function image(&$img) {
        $arFileTmp = CFile::ResizeImageGet(
                        $img['ID'], array("width" => $img['WIDTH'], "height" => $img['HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, true
        );
        $file = str_replace('/upload/iblock/', '/upload/watermark/iblock/', $arFileTmp['src']);
        $watermarkPath = $_SERVER["DOCUMENT_ROOT"] . $file;
//        preClear();
//        pre($file, $watermarkPath, file_exists($watermarkPath));
        if (!file_exists($watermarkPath)) {
            CheckDirPath($watermarkPath);
            $origPath = $_SERVER["DOCUMENT_ROOT"] . $arFileTmp['src'];

            $logoPath = $_SERVER["DOCUMENT_ROOT"] . self::PATH;
            $logo = $logoPath . self::LOGO;
            if (!file_exists($logo)) {
                $size = 60;
                $notice = self::NOTICE;
                exec($exec = "
convert -size 400x60 xc:grey30 -font Arial -pointsize 60 -gravity center \
          -draw \"fill grey70  text 0,0  '{$notice}'\" \
          {$logoPath}stamp_fgnd.png
 convert -size 400x60 xc:black -font Arial -pointsize 60 -gravity center \
          -draw \"fill white  text  1,1  '{$notice}'  \
                             text  0,0  '{$notice}'  \
                 fill black  text -1,-1 '{$notice}'\" \
          +matte {$logoPath}stamp_mask.png
composite -compose CopyOpacity  {$logoPath}stamp_mask.png  {$logoPath}stamp_fgnd.png  {$logoPath}stamp.png
mogrify -trim +repage {$logo}
");
//                pre($exec);
            }
            exec($exec = "composite -gravity SouthWest -geometry +15+15 {$logo} $origPath $watermarkPath");
//            pre($exec);
        }
        $img['SRC'] = $file;
    }

    static public function previewWatermakr($ID, $size = '.default') {
//        preClear();
        $file = CFile::GetByID($ID)->Fetch();
        application_watermark::image($file);
//        $file['SRC'] = $file['src'];
        $img = array(
            'FILE_NAME' => $file['FILE_NAME'],
            'SUBDIR' => 'watermark/' . $file['SUBDIR'],
            'WIDTH' => $file['WIDTH'],
            'HEIGHT' => $file['HEIGHT'],
            'CONTENT_TYPE' => $file['CONTENT_TYPE'],
        );
//        pre(cFile::GetByID ($ID)->Fetch());
//        pre($file, $img);
        $file2 = CFile::ResizeImageGet($img, array('width' => self::$mSize[$size][0], 'height' => self::$mSize[$size][1]), BX_RESIZE_IMAGE_EXACT);
//        preExit($file2);
        return $file2['src'];
    }

}
